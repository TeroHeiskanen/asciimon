
#include <curses.h>
#include "Enumerations.h"

using namespace std;

#ifndef ICON_H
#define ICON_H

class Icon{
private:
	int objIcon;
	COLOR objColor;
	COLOR backgroundColor;	
public:
	Icon();
	Icon(MONSTERTYPE type);
	Icon(WORLDOBJ type);
	COLOR getColor();
	int getIcon();
	void generateIcon();
};

#endif