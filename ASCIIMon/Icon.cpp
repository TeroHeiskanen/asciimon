#include "Icon.h"

using namespace std;



Icon::Icon(){
}

Icon::Icon(MONSTERTYPE type){

	backgroundColor=C_BLACK;

	switch(type){
		case MT_NORMAL:
			generateIcon();
			objColor=C_WHITE;
			break;
		case MT_FIRE:
			generateIcon();
			objColor=C_RED;
			break;
		case MT_WATER:
			generateIcon();
			objColor=C_BLUE;
			break;
		case MT_GRASS:
			generateIcon();
			objColor=C_GREEN;
			break;
		case MT_ELECTRIC:
			generateIcon();
			objColor=C_YELLOW;
			break;
		case MT_PSYCHIC:
			generateIcon();
			objColor=C_VIOLET;
			break;
		case MT_FIGHTING:
			generateIcon();
			objColor=C_GRAY;
			break;
		case MT_GROUND:
			generateIcon();
			objColor=C_BROWN;
			break;
		case MT_ICE:
			generateIcon();
			objColor=C_BRIGHT_CYAN;
			break;
		case MT_POISON:
			generateIcon();
			objColor=C_CYAN;
			break;
		case MT_BUG:
			generateIcon();
			objColor=C_BRIGHT_GREEN;
			break;		
	}
}

Icon::Icon(WORLDOBJ type){
	int temp = rand()%100;
	switch(type){
		case WO_FOREST:
			//if(temp < 50)
				objIcon='A';
			//else
			//	objIcon='A';
			objColor=C_GREEN;
			break;
		case WO_WATER:
			//if(temp < 50)
			//	objIcon=176;
			//else
			//objIcon=178;
			if(temp < 50)
				objIcon='~';
			else
				objIcon='^';
			objColor=C_BRIGHT_BLUE;
			break;
		case WO_WALL:
			//objIcon=178;
			objIcon='#';
			objColor=C_WHITE;
			break;
		case WO_GROUND:
			//objIcon=176;
			objIcon=',';
			objColor=C_BROWN;
			break;
		case WO_ROAD:
			//objIcon=177;
			objIcon='=';
			objColor=C_BROWN;
			break;
		case WO_BRIDGE:
			//objIcon=177;
			objIcon='=';
			objColor=C_GRAY;
			break;
		case WO_LARGE_ROAD:
			//objIcon=177;
			objIcon='=';
			objColor=C_BROWN;
			break;
		case WO_GRASS:
			objIcon=';';
			objColor=C_BRIGHT_GREEN;
			break;
		case WO_GRASS_ALT:
			objIcon='"';
			objColor=C_BRIGHT_GREEN;
			break;
		case WO_LAND:
			//objIcon=176;
			objIcon='.';
			objColor=C_BRIGHT_GREEN;
			break;
		case WO_LAND_ALT:
			//objIcon=176;
			objIcon=':';
			objColor=C_BRIGHT_GREEN;
			break;
		case WO_HI_LAND:
			//objIcon=176;
			objIcon=',';
			objColor=C_GREEN;
			break;
		case WO_HI_LAND_ALT:
			//objIcon=176;
			objIcon='o';
			objColor=C_GRAY;
			break;
		case WO_NPC:
			objIcon='R';
			objColor=C_RED;
			break;
		case WO_PLAYER:
			objIcon='@';
			objColor=C_RED;
			break;
		case WO_HP:
			objIcon=220;
			objColor=C_GREEN;
			break;
		case WO_HPLOST:
			objIcon=220;
			objColor=C_RED;
			break;
		case WO_FLOOR:
			objIcon=176;
			objColor=C_WHITE;
			break;
		case WO_ROOF_NORMAL_HOUSE:
			//objIcon=178;
			objIcon='#';
			objColor=C_BROWN;
			break;
		case WO_ROOF_ASCIICENTER:
			//objIcon=178;
			objIcon='#';
			objColor=C_BRIGHT_RED;
			break;
		case WO_ROOF_SPAWN:
			//objIcon=178;
			objIcon='#';
			objColor=C_BROWN;
			break;
		case WO_ROOF_GYM:
			//objIcon=178;
			objIcon='#';
			objColor=C_BROWN;
			break;
		case WO_ROOF_SHOP:
			//objIcon=178;
			objIcon='#';
			objColor=C_YELLOW;
			break;
		case WO_DOOR_NORMAL_HOUSE:
			objIcon='D';
			objColor=C_BROWN;
			break;
		case WO_DOOR_ASCIICENTER:
			objIcon='M';
			objColor=C_BROWN;
			break;
		case WO_DOOR_SPAWN:
			objIcon='D';
			objColor=C_BROWN;
			break;
		case WO_DOOR_GYM:
			objIcon='D';
			objColor=C_BROWN;
			break;
		case WO_DOOR_SHOP:
			objIcon='S';
			objColor=C_BROWN;
			break;
		case WO_SAND:
			//objIcon=176;
			objIcon='~';
			objColor=C_YELLOW;
			break;
		case WO_DEEP_WATER:
			//objIcon=178;
			if(temp < 50)
				objIcon='~';
			else
				objIcon='^';
			objColor=C_BLUE;
			break;
		case WO_HILL:
			objIcon='^';
			objColor=C_GRAY;
			break;
		case WO_SNOW:
			//objIcon=176;
			objIcon='~';
			objColor=C_WHITE;
			break;
		case WO_DEBUG:
			objIcon='.';
			objColor=C_WHITE;
			break;
		default:
			cout<<"ENUM PUUTTUU"<<endl;
			break;	
	}
}

void Icon::generateIcon(){
	do{
	objIcon = rand()%257+33;
	}while(objIcon == 255 || objIcon == 256 || objIcon == 288);
}

int Icon::getIcon(){
	return objIcon;
}

COLOR Icon::getColor(){
	return objColor;
}

