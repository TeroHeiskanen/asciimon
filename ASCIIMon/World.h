#include "Enumerations.h" 
#include "PerlinNoise.h" 
#include "Building.h" 
#include <vector> 
#include <thread> 
  
#ifndef WORLD_H 
#define WORLD_H 
  
struct RoadNode{ 
    int x, y; 
    RoadNode *parent; 
    int F; 
    int G; 
    int H; 
    bool open; 
    bool closed; 
  
    RoadNode(){ 
        open = false; 
        closed = false; 
    } 
  
    RoadNode(int x, int y, RoadNode *parent){ 
        this->x = x; 
        this->y = y; 
        this->parent = parent; 
        open = true; 
        closed = false; 
    }    
}; 
  
struct NodePointer{ 
    RoadNode *ptr; 
  
    NodePointer(RoadNode *ptr){ 
        this->ptr = ptr; 
    } 
  
    bool operator<(const NodePointer &o) const{ 
        if(ptr->F < o.ptr->F) 
            return true; 
        else
            return false; 
    } 
  
    bool operator()(const NodePointer &left, const NodePointer &right) const{ 
        if(left.ptr->F > right.ptr->F) 
            return true; 
        else
            return false; 
    } 
}; 
  
struct Coordinate{ 
    int x,y; 
      
    Coordinate(int x, int y){ 
        this->x = x; 
        this->y = y; 
    } 
  
    bool operator<(const Coordinate &o) const{ 
        if(x == o.x){ 
            return y < o.y; 
        } 
        return x < o.x; 
    } 
  
    bool operator()(const Coordinate &left, const Coordinate &right) const{ 
        if(left.x > right.x) 
            return true; 
        if(left.x < right.x) 
            return false; 
        return (left.y > right.y); 
    } 
  
    bool operator==(const Coordinate &o) const{ 
        if(x == o.x && y == o.y) 
            return true; 
        else
            return false; 
    } 
}; 
  
class WorldBlock{ 
private: 
    WORLDOBJ block; 
    Building *building; 
public: 
    WorldBlock(); 
    WorldBlock(Building *building); 
    WorldBlock(WORLDOBJ block); 
    Building* getBuilding(); 
    bool isBuilding(); 
    bool isPassable(); 
    WORLDOBJ getBlock(); 
}; 
  
class World{ 
private: 
    int size; 
    int roadDirection; 
    int debugX, debugY; 
    int spawnX, spawnY; 
    int randomseed; 
    int waterLevel; 
    int deepWaterLevel; 
    int hillLevel; 
    int snowLevel; 
    int landLevel; 
    int hiLandLevel; 
    int sandLevel; 
    int forestLevel; 
    int grassLevel; 
    PerlinNoise *pn; 
    WorldBlock **map; 
    vector<City> cities; 
    vector<City*> connectedCities; 
    vector<thread> roadThreads; 
    City spawnCity; 
    vector<vector<int>> heightmap,forestMap,grassMap; 
    vector<int> heights; 
  
    WORLDOBJ getTileType(int height); 
    double islandGradient(int x, int y); 
    void mapThread(vector<vector<int>> *v, int x1, int x2); 
    int calculateCost(RoadNode *node, RoadNode *parent); 
    int calculateHeuristic(RoadNode *node, int goalX, int goalY); 
    bool formatNode(RoadNode *node, RoadNode *parent, int x, int y, int goalX, int goalY); 
    bool isAdjacentPenaltyNode(Coordinate c); 
    bool isPenaltyNode(Coordinate c); 
    void checkNode(RoadNode *node, RoadNode *parent); 
    void generateLand(double persistence, double frequency, int octaves); 
    void generateCities(int count); 
    void connectCities(); 
    void connectNearestUnconnected(); 
    bool addToConnected(City* c); 
    void findConnected(City* c); 
    bool isConnected(City *c); 
    City* nearestCity(City *c); 
    City* nearestUnconnectedCity(City *c); 
    void generateCity(City *city); 
    void generateSpawn(); 
    void generateBuilding(Building *building); 
    void generateBuilding(int x, int y, Building *building); 
    void generateRoad(Coordinate cStart, Coordinate cGoal, bool cityRoad); 
    bool validForRoad(int x, int y); 
    void generateForestAndGrass(int percentage, double persistence, double frequency, int octaves); 
    void mergeMapWithForestAndGrass(); 
    bool isAllowedForForest(int x, int y); 
    bool isAllowedForGrass(int x, int y); 
    void smoothenMap();  
    void calculateLevels(); 
    bool isPassable(int x, int y); 
    bool isPassableArea(int x, int y, int offset); 
    bool citiesWithinDistance(int x, int y, int d); 
    int distance(int x1, int y1, int x2, int y2); 
    void getSpawn(int x, int y, int orient); 
public: 
    ~World(); 
    World(); 
    World(int size); 
    void generate(int randomseed); 
    WORLDOBJ getBlock(int y, int x); 
    void makeMapBmp(string path); 
    void makeHMapBmp(); 
    void makePNHMapBmp(); 
	int getSpawnX();
	int getSpawnY();
}; 
#endif