#include "Enumerations.h"
#include <vector>

#ifndef BUILDING_H
#define BUILDING_H

class Building{
protected:
	WORLDOBJ **inside, **outside;
	HOUSE buildingType;
	int sizeX, sizeY;
	int doorX, doorY;
	int locationX, locationY;
public:
	Building();
	Building(HOUSE type, int sizeX, int sizeY, int locationX, int locationY);
	Building(HOUSE type, int sizeX, int sizeY, int doorX, int doorY, int locationX, int locationY);
	int getSizeX();
	int getSizeY();
	int getLocationX();
	int getLocationY();
	int getDoorX();
	int getDoorY();
	WORLDOBJ getTileInside(int x, int y);
	WORLDOBJ getTileOutside(int x, int y);
	HOUSE getBuildingType();
};

class ASCIICenter:public Building{
public:
	ASCIICenter(int locationX, int locationY);
};

class NormalHouse:public Building{
public:
	NormalHouse(int locationX, int locationY);
};

class SpawnHouse:public Building{
public:
	SpawnHouse(int locationX, int locationY);
};

class Gym:public Building{
public:
	Gym(int locationX, int locationY);
};

class Shop:public Building{
public:
	Shop(int locationX, int locationY);
};

class City{
protected:
	CITY cityType;
	std::vector<City*> citiesConnected;
	Building *buildings;
	int roadX, roadY;
	int buildingCount;
	int radius;

	bool isValidPosition(Building building1, Building building2);
	int distance(int x1, int y1, int x2, int y2);
public:
	City();
	City(CITY cityType, int radius, int roadX, int roadY);
	int getRadius();
	int getRoadX();
	int getRoadY();
	int getBuildingCount();
	Building* getBuilding(int index);	
	CITY getCityType();
	void addConnection(City *city);
	void shareConnections(City *city);
	int getConnectionCount();
	City* getConnection(int index);
	bool isConnected(City *city);

	bool operator==(City &o);
	bool operator!=(City &o);
};

class SpawnCity:public City{
public:
	SpawnCity(int x, int y);
};

class SmallCity:public City{
public:
	SmallCity(int x, int y);
};

class MediumCity:public City{
public:
	MediumCity(int x, int y);
};

class LargeCity:public City{
public:
	LargeCity(int x, int y);
};
#endif