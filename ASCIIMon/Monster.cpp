#include "Monster.h"
#include <random>
#include <iostream>
#include <map>
#include "Variables.h"

using namespace std;

char generateVowel();
char generateConsonant();
bool generateBool();
string generateName();

const char vowel[] = {'A','E','I','O','U','Y'};
const char consonants[] = {'B','C','D','F','G','H','J','K','L','M','N','P','Q','R','S','T','V','W','X','Z'};

//MONSTERBASE

MonsterBase::MonsterBase(){

}

MonsterBase::MonsterBase(string name){
	this->name = name;

	int temp = rand()%11+1;
	if(temp == 1){
		monsterType = MT_NORMAL;
	}
	else if(temp == 2){
		monsterType = MT_FIRE;
	}
	else if(temp == 3){
		monsterType = MT_WATER;
	}
	else if(temp == 4){
		monsterType = MT_GRASS;
	}
	else if(temp == 5){
		monsterType = MT_ELECTRIC;
	}
	else if(temp == 6){
		monsterType = MT_PSYCHIC;
	}
	else if(temp == 7){
		monsterType = MT_FIGHTING;
	}
	else if(temp == 8){
		monsterType = MT_GROUND;
	}
	else if(temp == 9){
		monsterType = MT_ICE;
	}
	else if(temp == 10){
		monsterType = MT_POISON;
	}
	else if(temp == 11){
		monsterType = MT_BUG;
	}

	attMulti = (rand()%4)+1;
	defMulti = (rand()%4)+1;
	spdMulti = (rand()%3)+1;
	hpMulti = 12-(attMulti+defMulti+spdMulti)+(rand()%3);

	monsterIcon = Icon(monsterType);
	monsterIcon.generateIcon();
}

MonsterBase::MonsterBase(const MonsterBase &obj){
	name = obj.name;
	attMulti = obj.attMulti;
	defMulti = obj.defMulti;
	spdMulti = obj.spdMulti;
	hpMulti = obj.hpMulti;
	monsterIcon = obj.monsterIcon;
	monsterType = obj.monsterType;
}

int MonsterBase::getAttMulti(){
	return attMulti;
}

int MonsterBase::getDefMulti(){
	return defMulti;
}

int MonsterBase::getSpdMulti(){
	return spdMulti;
}

int MonsterBase::getHpMulti(){
	return hpMulti;
}

string MonsterBase::getName(){
	return name;
}

MONSTERTYPE MonsterBase::getType(){
	return monsterType;
}

Icon MonsterBase::getIcon(){
	return monsterIcon;
}

void MonsterBase::print(){
	string temp = "TYPE = ";
	if(monsterType == MT_WATER){
		temp += "WATER";
	}
	else if(monsterType == MT_FIRE){
		temp += "FIRE";
	}
	else if(monsterType == MT_GRASS){
		temp += "GRASS";
	}
	else if(monsterType == MT_ELECTRIC){
		temp += "ELECTRIC";
	}
	else if(monsterType == MT_NORMAL){
		temp += "NORMAL";
	}
	cout<<"NAME = "<<name<<endl;
	cout<<temp<<endl;
	cout<<"ATT  = "<<attMulti<<endl;
	cout<<"DEF  = "<<defMulti<<endl;
	cout<<"SPD  = "<<spdMulti<<endl;
	cout<<"HP   = "<<hpMulti<<endl;
}

//MONSTERS

Monsters::Monsters(int count){
	m = new MonsterBase[count+1];
	this->count = count;

	map<string,bool> nameMap;
	string name;
	
	for(int i = 0 ; i <= count ; i++){
		do{
			name = generateName();
		}while(nameMap[name]);
		m[i] = MonsterBase(name);
	}
}

MonsterBase Monsters::get(int index){
	if(index < count){
		return m[index];
	}
	else{
		return NULL;
	}
}

MonsterBase Monsters::getRandom(){
	return m[rand()%count];
}

int Monsters::getCount(){
	return count;
}

//MONSTERSET

MonsterSet::MonsterSet(){
	count = 0;
	maxSize = mSet.max_size();
}

MonsterSet::MonsterSet(int maxSize){
	count = 0;
	this->maxSize = maxSize;
}

void MonsterSet::addMonster(ASCIIMonster m){
	if(count < maxSize){
		mSet[count] = m;
		count++;
	}
}

void MonsterSet::deleteMonster(int index){
	if(index < count){
		for(int i = index ; i < (count-1) ; i++){
			mSet[i] = mSet[i+1];
		}
		mSet.erase(count-1);
		count--;
	}
}

void MonsterSet::switchMonsters(int index1, int index2){
	if(index1 < count && index1 > -1 && index2 < count && index2 > -1){
		ASCIIMonster temp = mSet[index1];
		mSet[index1] = mSet[index2];
		mSet[index2] = temp;
	}	
}

bool MonsterSet::isEmpty(){
	if(count == 0)
		return true;
	else
		return false;
}

bool MonsterSet::isFull(){
	if(count == maxSize)
		return true;
	else
		return false;
}

ASCIIMonster* MonsterSet::getMonster(int index){
	if(index < count)
		return &mSet[index];
	else
		return NULL;
}

int MonsterSet::getCount(){
	return count;
}

int MonsterSet::getMaxSize(){
	return maxSize;
}

//ASCIIMON

ASCIIMonster::ASCIIMonster(){
}

ASCIIMonster::ASCIIMonster(MonsterBase monster, int lvl): MonsterBase(monster){
	att = 0;
	def = 0;
	spd = 0;
	maxhp = 0;
	this->lvl = 0;
	exp = int(pow(lvl-1,2)*2)*100;

	for(int i = 0 ; i < 4 ; i++){
		moves[i] = NULL;
	}

	for(int i = 0 ; i < lvl ; i++){
		levelUp();
	}
}

bool ASCIIMonster::addMove(Move* m){
	for(int i = 0 ; i < 4 ; i++){
		if(moves[i] == NULL){
			moves[i] = m;
			return true;
		}
	}
	return false;
}

bool ASCIIMonster::doesExist(Move* m){
	for(int i = 0 ; i < 4 ; i++){
		if(moves[i] == NULL){
			return false;
		}
		else if(moves[i]->getName() == m->getName()){
			return true;
		}
	}
	return false;
}

void ASCIIMonster::delMove(int index){
	moves[index] = NULL;
}

void ASCIIMonster::switchMove(int index1, int index2){
	if(moves[index1] != NULL && moves[index2] != NULL){
		Move* temp;
		temp = moves[index1];
		moves[index1] = moves[index2];
		moves[index2] = temp;
	}
}

void ASCIIMonster::getStartingMoves(){
	int howMany = rand()%3+1;
	Move *temp;

	for(int i = 0 ; i < howMany ; i++){
		for(int j = 0 ; j < 10 ; j++){
			temp = globalMoves->getStartingMove(monsterType, lvl);
			if(temp != NULL && !doesExist(temp)){
				moves[i] = temp;
				break;
			}
		}
		if(moves[i] == NULL){
			break;
		}
	}
	if(moves[0] == NULL){
		if(rand()%100 > 49)
			addMove(globalMoves->getMove(monsterType, 1));
		else
			addMove(globalMoves->getMove(MT_NORMAL, 1));
	}
}

void ASCIIMonster::levelUp(){
	att += MonsterBase::getAttMulti() + rand()%3-1;
	def += MonsterBase::getDefMulti() + rand()%3-1;
	spd += MonsterBase::getSpdMulti() + rand()%3-1;
	maxhp += MonsterBase::getHpMulti() + rand()%3-1;
	lvl++;
	addMove(globalMoves->getMove(monsterType, lvl));
}

bool ASCIIMonster::checkLevelUp(){
	int nextLvlAt = int(pow(lvl,2)*2)*100;
	if(exp >= nextLvlAt){
		levelUp();
		return true;
	}
	else
		return false;
}

void ASCIIMonster::addExp(int exp){
	this->exp += exp;
}

int ASCIIMonster::getAtt(){
	return att;
}

int ASCIIMonster::getDef(){
	return def;
}

int ASCIIMonster::getSpd(){
	return spd;
}

int ASCIIMonster::getHp(){
	return hp;
}

int ASCIIMonster::getMaxhp(){
	return maxhp;
}

int ASCIIMonster::getLvl(){
	return lvl;
}

int ASCIIMonster::getExp(){
	return exp;
}

void ASCIIMonster::setHpToMax(){
	hp = maxhp;
}

Move* ASCIIMonster::getMove(int index){
	if(index >= 0 && index < 5)
		return moves[index];
	else
		return NULL;
}

void ASCIIMonster::receiveDmg(int dmg){
	if(dmg > hp)
		hp = 0;
	else
		hp -= dmg;
}

void ASCIIMonster::print(){
	MONSTERTYPE type = MonsterBase::getType();
	cout<<"NAME = "<<MonsterBase::getName()<<endl;	
	cout<<"TYPE = "<<type<<endl;
	cout<<"LVL  = "<<lvl<<endl;
	cout<<"ATT  = "<<att<<endl;
	cout<<"DEF  = "<<def<<endl;
	cout<<"SPD  = "<<spd<<endl;
	cout<<"HP   = "<<maxhp<<endl;
	cout<<"EXP  = "<<exp<<endl;
}

//NAMEGENERATOR

string generateName(){
	string name;
	int length;	
	
	int lastVowels = 0;
	int lastConsonants = 0;

	length = rand()%3 + 2;

	for(int i = 0 ; i <= length ; i++){
		if(i == 0){
			if(generateBool()){
				name.append(1,generateVowel());
				lastVowels++;
				continue;
			}else{
				name.append(1,generateConsonant());
				lastConsonants++;
				continue;
			}
		}
		if(lastVowels == 2){
			name.append(1,generateConsonant());
			lastConsonants++;
			lastVowels = 0;
			continue;
		}
		if(lastConsonants == 1){
			name.append(1,generateVowel());
			lastVowels++;
			lastConsonants = 0;
			continue;
		}
		if(generateBool()){
			name.append(1,generateVowel());
			lastVowels++;
			lastConsonants = 0;
		}else{
			name.append(1,generateConsonant());
			lastConsonants++;
			lastVowels = 0;
		}		
	}
	name.append("MON");
	return name;
}

bool generateBool(){
	if((rand()%10+1) > 5)
		return true;
	else
		return false;
}

char generateVowel(){
	return vowel[rand()%6];
}

char generateConsonant(){	
	return consonants[rand()%20];
}