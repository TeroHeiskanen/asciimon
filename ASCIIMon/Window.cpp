#include "Window.h"
#include <time.h>

using namespace std;

Window::Window(){

	initialize();

	////////////////////////////
	while(1){
		
		if(worldWinActive){

			//info();
			drawWorld(playerPositionX - worldWin->_maxx/2, playerPositionY - worldWin->_maxy/2);
			playerMove();

		}

		if(combatWinActive){
			drawCombat();
			wrefresh(combatWin);
			getKeyPress();
		}
		
	}
	////////////////////////////

	endwin();
}

void Window::initialize(){
	initscr();
	srand(time(NULL));

	getmaxyx(stdscr, maxHeight,maxWidth);
	delwin(stdscr);

	worldWin = newwin(15,30,0,0);
	combatWin = newwin(15,20,0,29);
	infoWin = newwin(7,49,14,0);

	playerPositionX = worldMap->getSpawnX();
	playerPositionY = worldMap->getSpawnY();

	keypad(worldWin,true);
	noecho();
	curs_set(0);

	initColors();
	
	system("mode 49, 21");

	borders();

	x = 10;
	y = 10;
	menuX = 2;
	menuY = 11;
	menuMX = 2;
	menuMY = 2;
	seed = rand()%20000;

	combatWinActive = false;
	worldWinActive = true;
	monMenuActive = false;
	monMenuStatsActive = false;
	fightMenuActive = false;
	idMenuActive = false;
	mainMenuActive = true;
	fightMenuActive = false;
	monListMiniMenuActive = false;

	playerSet = new MonsterSet(6);
	//enemy = new ASCIIMonster(globalMonsters->getRandom(), rand()%98+1);

	for(int i = 0 ; i < 6 ; i++){
		playerSet->addMonster(ASCIIMonster(globalMonsters->getRandom(),5));
		playerSet->getMonster(i)->getStartingMoves();
		playerSet->getMonster(i)->setHpToMax();
	}

	wrefresh(worldWin);
	wrefresh(combatWin);
	wrefresh(infoWin);

	activePlayerMon = 0;
}

void Window::drawWorld(int playerPositionX, int playerPositionY){
	for(int i = 1 ; i < worldWin->_maxx -1 ; i++){
		for(int j = 1 ; j < worldWin->_maxy -1 ; j++){
			print(worldWin,i,j,Icon(worldMap->getBlock(playerPositionX + i,playerPositionY + j)));
			//print(win,j,i,Icon(WO_DEBUG));
		}
	}
	print(worldWin, worldWin->_maxx/2, worldWin->_maxy/2, Icon(WO_PLAYER));	
	wrefresh(worldWin);
}

void Window::playerMove(){
	getKeyPress();
	if(collisionDetect(direction, playerPositionX, playerPositionY)){
		if(direction=='l'){
			playerPositionX--;
		}else if(direction=='r'){
			playerPositionX++;
		}else if(direction=='u'){
			playerPositionY--;
		}else if(direction=='d'){
			playerPositionY++;
		}else if(direction=='b'){
			activateBattle(activePlayerMon);
		}
	}

	if(worldMap->getBlock(playerPositionX, playerPositionY) == WO_GRASS || worldMap->getBlock(playerPositionX, playerPositionY) == WO_GRASS_ALT){
		if(rand()%100 > 90){
		wrefresh(worldWin);
		activateBattle(activePlayerMon);
		}
	}
	
}

void Window::borders(){
	wattron(worldWin,COLOR_PAIR(1));
	wborder(worldWin,186, 186, 205, 205, 201, 187, 201, 188);
	wattroff(worldWin,COLOR_PAIR(1));

	wattron(combatWin,COLOR_PAIR(1));
	wborder(combatWin,186, 186, 205, 205, 203, 187, 202, 185);
	wattroff(combatWin,COLOR_PAIR(1));

	wattron(infoWin,COLOR_PAIR(1));	
	wborder(infoWin,186, 186, 205, 205, 201, 187, 200, 188);	
	wattroff(infoWin,COLOR_PAIR(1));
}

void Window::print(WINDOW *win,int x, int y, Icon icon){
	
	switch(icon.getColor()){
	case C_WHITE:
		wattron(win,COLOR_PAIR(2));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(2));
		break;
	case C_RED:
		wattron(win,COLOR_PAIR(3));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(3));
		break;
	case C_BLUE:
		wattron(win,COLOR_PAIR(4));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(4));
		break;
	case C_GREEN:
		wattron(win,COLOR_PAIR(5));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(5));
		break;
	case C_BROWN:
		wattron(win,COLOR_PAIR(6));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(6));
		break;
	case C_VIOLET:
		wattron(win,COLOR_PAIR(7));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(7));
		break;
	case C_YELLOW:
		wattron(win,COLOR_PAIR(8));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(8));
		break;
	case C_BRIGHT_RED:
		wattron(win,COLOR_PAIR(9));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(9));
		break;
	case C_BRIGHT_CYAN:
		wattron(win,COLOR_PAIR(10));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(10));
		break;
	case C_CYAN:
		wattron(win,COLOR_PAIR(11));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(11));
		break;
	case C_BRIGHT_GREEN:
		wattron(win,COLOR_PAIR(12));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(12));
		break;
	case C_BRIGHT_BLUE:
		wattron(win,COLOR_PAIR(13));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(13));
		break;
	case C_GRAY:
		wattron(win,COLOR_PAIR(14));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(14));
		break;
	case C_BLACK:
		wattron(win,COLOR_PAIR(15));
		mvwaddch(win, y,x, icon.getIcon());
		wattroff(win,COLOR_PAIR(15));
		break;
	}
	
}

void Window::drawOpponent(ASCIIMonster *m){

		float hp = m->getHp();
		float maxhp = m->getMaxhp();
		int suhde = 8*(hp/maxhp)+2;

		for(int j = 13 ; j < 18 ; j++)
			mvwaddch(combatWin, 3,j, battleFloor); 

		mvwprintw(combatWin, 2, 2, m->getName().c_str());

		for(int i = 2 ; i < suhde ; i++)
			print(combatWin, i, 3, WO_HP);
		for(int i = 9 ; i >= suhde ; i--)
			print(combatWin, i, 3, WO_HPLOST);
		

		print(combatWin, 15, 2, m->getIcon()); 
		
}

void Window::drawPlayer(ASCIIMonster *m){

		for(int j = 2 ; j < 7 ; j++)
			mvwaddch(combatWin, 7,j, battleFloor); 

		mvwprintw(combatWin, 6, 10, m->getName().c_str());

		for(int i = 10 ; i < 18 ; i++)
			print(combatWin, i, 7, WO_HP);

		print(combatWin, 4, 6, m->getIcon()); 

		for(int i = 1 ; i < 19 ; i++){
			wattron(combatWin,COLOR_PAIR(1));
			mvwaddch(combatWin, 9, i, 205);
			wattroff(combatWin,COLOR_PAIR(1));
		}
}

void Window::drawCombat(){	

	if(!monMenuActive && !monListMiniMenuActive){
		clearCombatWin();
		drawOpponent(enemy);
		drawPlayer(playerSet->getMonster(activePlayerMon));
		wrefresh(combatWin);
	}

	if(mainMenuActive){
		drawCombatMenu();
	}

	if(fightMenuActive){
		combat();
	}

	if(monMenuActive){
		drawMonList(playerSet);
		drawMonListCursor();
	}
	if(monListMiniMenuActive){
		drawMonListMiniMenu();
	}

}

void Window::drawMonList(MonsterSet *playerSet){

	clearCombatWin();
	clearCombatMenu();

	int j = 2;
	for(int i = 0 ; i < playerSet->getCount() ; i++){
	print(combatWin, 3, j, playerSet->getMonster(i)->getIcon());
	mvwprintw(combatWin, j, 5, playerSet->getMonster(i)->getName().c_str());
	j = j + 2;
	}
}

void Window::drawMonListCursor(){

	if(direction=='x' && monMenuActive && !monMenuStatsActive){
		clearCombatWin();
		clearCombatMenu();

		//suboptimal
		drawOpponent(enemy);
		drawPlayer(playerSet->getMonster(activePlayerMon));
		drawCombatMenu();

		mainMenuActive = true;
		monMenuActive = false;
	}else if(direction=='u' && menuMY != 2){
		menuMY = menuMY - 2;
	}else if(direction=='d' && menuMY < playerSet->getCount()*2){
		menuMY = menuMY + 2;
	}else if(direction=='z' && monMenuActive && !monMenuStatsActive){
		direction = NULL;
		miniMenuY = 12;
		monListMiniMenuActive = true;
		monMenuActive = false;
	}else if(direction=='x' && monMenuStatsActive){
			monMenuStatsActive = false;
			monMenuStatsPage = true;
			direction = NULL;
	}

	if(monMenuStatsActive){
	drawPlayerMon(menuMY/2-1);
	}

	if(monMenuActive && !monMenuStatsActive)
	mvwaddch(combatWin, menuMY, menuMX, '>');

}

void Window::drawCombatMenu(){


	if(direction=='l' && menuX != 2){
		menuX = 2;
	}else if(direction=='r' && menuX != 10){
		menuX = 10;
	}else if(direction=='u' && menuY != 11){
		menuY = 11;
	}else if(direction=='d' && menuY != 13){
		menuY = 13;
	}else if(direction=='z' && menuY == 13 && menuX == 2){
		menuMY = 2;
		monMenuActive = true;
		mainMenuActive = false;
		direction = NULL;
	}else if(direction=='z' && menuY == 13 && menuX == 10){
		runAway();
	}else if(direction=='z' && menuY == 11 && menuX == 10){
		drawEnemyMon();
		//suboptimal
		drawOpponent(enemy);
		drawPlayer(playerSet->getMonster(activePlayerMon));
	}else if(direction=='z' && menuY == 11 && menuX == 2){
		direction = NULL;
		menuCY = 10;
		fightMenuActive = true;
		mainMenuActive = false;
	}

	if(combatWinActive){
		clearCombatMenu();		

		mvwaddch(combatWin, menuY, menuX, '>');
		mvwprintw(combatWin, 11, 3, "FIGHT");
		mvwprintw(combatWin, 11, 11, "ID");
		mvwprintw(combatWin, 13, 3, "MON");
		mvwprintw(combatWin, 13, 11, "RUN");
	}
}

void Window::drawEnemyMon(){

		clearCombatWin();
		clearCombatMenu();

		int hp = enemy->getHp();
		int maxHp = enemy->getMaxhp();
		int lvl = enemy->getLvl();
		int att = enemy->getAtt();
		int def = enemy->getDef();
		int spd = enemy->getSpd();
		float exp = enemy->getExp();

		print(combatWin, getmaxx(combatWin)/2-1, 2, enemy->getIcon());
		mvwprintw(combatWin, 4, 2, "NAME: ");
		mvwprintw(combatWin, 4, 9, enemy->getName().c_str());
		mvwprintw(combatWin, 5, 2, "HP:  ");
		mvwprintw(combatWin, 5, 9, "%d",hp);
		mvwprintw(combatWin, 5, 9+strlen(to_string(hp).c_str()), "/");
		mvwprintw(combatWin, 5, 10+strlen(to_string(hp).c_str()), "%d",maxHp);
		mvwprintw(combatWin, 6, 2, "LVL:  ");
		mvwprintw(combatWin, 6, 9, "%d",lvl);
		mvwprintw(combatWin, 7, 2, "ATT:  ");
		mvwprintw(combatWin, 7, 9, "%d",att);
		mvwprintw(combatWin, 8, 2, "DEF:  ");
		mvwprintw(combatWin, 8, 9, "%d",def);
		mvwprintw(combatWin, 9, 2, "SPD:  ");
		mvwprintw(combatWin, 9, 9, "%d",spd);

		wrefresh(combatWin);

		while(getch()!='x'){
		}

		clearCombatWin();
		clearCombatMenu();
}

void Window::drawPlayerMon(int m){

	if(direction=='l' && !monMenuStatsPage){
		monMenuStatsPage = true;
		direction=NULL;
	}else if(direction=='r' && monMenuStatsPage){
		monMenuStatsPage = false;
		direction=NULL;
	}

	if(monMenuStatsPage){
		int hp = playerSet->getMonster(m)->getHp();
		int maxHp = playerSet->getMonster(m)->getMaxhp();
		int lvl = playerSet->getMonster(m)->getLvl();
		int att = playerSet->getMonster(m)->getAtt();
		int def = playerSet->getMonster(m)->getDef();
		int spd = playerSet->getMonster(m)->getSpd();
		int exp = playerSet->getMonster(m)->getExp();
		float expThisLvl = int(pow(lvl-1,2)*2)*100;
		float expNextLvl = int(pow(lvl,2)*2)*100;
		int expPercentage = ((exp-expThisLvl)/(expNextLvl-expThisLvl))*100;

		clearCombatMenu();
		clearCombatWin();

		print(combatWin, getmaxx(combatWin)/2-1, 2, playerSet->getMonster(m)->getIcon());
		mvwprintw(combatWin, 4, 2, "NAME: ");
		mvwprintw(combatWin, 4, 9, playerSet->getMonster(m)->getName().c_str());
		mvwprintw(combatWin, 5, 2, "HP:  ");
		mvwprintw(combatWin, 5, 9, "%d",hp);
		mvwprintw(combatWin, 5, 9+strlen(to_string(hp).c_str()), "/");
		mvwprintw(combatWin, 5, 10+strlen(to_string(hp).c_str()), "%d",maxHp);
		mvwprintw(combatWin, 6, 2, "LVL:  ");
		mvwprintw(combatWin, 6, 9, "%d",lvl);
		mvwprintw(combatWin, 7, 2, "ATT:  ");
		mvwprintw(combatWin, 7, 9, "%d",att);
		mvwprintw(combatWin, 8, 2, "DEF:  ");
		mvwprintw(combatWin, 8, 9, "%d",def);
		mvwprintw(combatWin, 9, 2, "SPD:  ");
		mvwprintw(combatWin, 9, 9, "%d",spd);
		mvwprintw(combatWin, 10, 2, "EXP:  ");
		//mvwprintw(combatWin, 10, 9, "%d",exp);
		//mvwprintw(combatWin, 11, 9, "%d",int(pow(lvl,2)*2)*100);
		mvwprintw(combatWin, 10, 9, "%d",expPercentage);
		mvwaddch(combatWin, 10, 9+strlen(to_string(expPercentage).c_str()), 37);
		mvwprintw(combatWin, 13, 12, "MOVES->");
		wrefresh(combatWin);
	}else if(!monMenuStatsPage){

		clearCombatWin();
		clearCombatMenu();

		mvwprintw(combatWin, 2, 8, "DMG ACC PP");

		for(int i = 0 ; i < 4 ; i++){
			if(playerSet->getMonster(m)->getMove(i) != NULL){
				int dmg = playerSet->getMonster(m)->getMove(i)->getDmg();
				int acc = playerSet->getMonster(m)->getMove(i)->getAccuracy();
				int pp = playerSet->getMonster(m)->getMove(i)->getPp();
				mvwprintw(combatWin, 4+i*2, 2, playerSet->getMonster(m)->getMove(i)->getName().c_str());
				mvwprintw(combatWin, 5+i*2, 8, "%d", dmg);
				mvwprintw(combatWin, 5+i*2, 12, "%d", acc);
				mvwprintw(combatWin, 5+i*2, 16, "%d", pp);
				mvwprintw(combatWin, 13, 1, "<-STATS");
			}
		}
	}

}

void Window::drawMonListMiniMenu(){

	mvwprintw(combatWin, miniMenuY, 11, " ");

	for(int i = 11 ; i < 14 ; i++){
			wattron(combatWin,COLOR_PAIR(1));
			mvwaddch(combatWin, i, 10, 186);
			wattroff(combatWin,COLOR_PAIR(1));
		}
		for(int i = 10 ; i < 19 ; i++){
			wattron(combatWin,COLOR_PAIR(1));
			mvwaddch(combatWin, 11, i, 205);
			wattroff(combatWin,COLOR_PAIR(1));
		}
			wattron(combatWin,COLOR_PAIR(1));
			mvwaddch(combatWin, 11, 10, 201);
			wattroff(combatWin,COLOR_PAIR(1));

	if(direction == 'u' && miniMenuY != 12){
		miniMenuY = 12;
	}else if(direction == 'd' && miniMenuY != 13){
		miniMenuY = 13;
	}else if(direction == 'z' && miniMenuY == 12){
		direction = NULL;
		monMenuActive = true;
		monMenuStatsActive = true;
		monListMiniMenuActive = false;
		//suboptimal
		clearCombatMenu();
		clearCombatWin();
		drawMonListCursor();
		
	}else if(direction == 'z' && miniMenuY == 13){
		direction = NULL;
		playerSet->switchMonsters(0, menuMY/2-1);
		activePlayerMon = 0;
		monMenuActive = true;
		monListMiniMenuActive = false;
		//suboptimal
		clearCombatMenu();
		clearCombatWin();
		drawMonList(playerSet);
		drawMonListCursor();
	}else if(direction == 'x'){
		direction = NULL;
		monMenuActive = true;
		monListMiniMenuActive = false;
		//suboptimal
		clearCombatMenu();
		clearCombatWin();
		drawMonList(playerSet);
		drawMonListCursor();
	}

	if(monListMiniMenuActive){
		mvwprintw(combatWin, miniMenuY, 11, ">");
		mvwprintw(combatWin, 12, 12, " STATS");
		mvwprintw(combatWin, 13, 12, " SWITCH");
	}
	wrefresh(combatWin);
}

void Window::drawMoves(){
	clearCombatMenu();
	moveCount = 0;

	for(int i = 0; i < 4 ; i++){
		if(playerSet->getMonster(activePlayerMon)->getMove(i) != NULL){
			mvwprintw(combatWin, 10+i, 3, playerSet->getMonster(activePlayerMon)->getMove(i)->getName().c_str());
			mvwprintw(combatWin, 10+i, 17, "%d",playerSet->getMonster(activePlayerMon)->getMove(i)->getPp());
			moveCount++;
		}
	}
}

void Window::clearCombatMenu(){
	for(int i = 1 ; i < 19 ; i++){
		for(int j = 10 ; j < 14 ; j++){
			mvwprintw(combatWin, j,i," ");
		}
	}
}

void Window::clearCombatWin(){
	for(int i = 1 ; i < 19 ; i++){
		for(int j = 1 ; j < 10 ; j++){
			mvwprintw(combatWin, j,i," ");
		}
	}
	wrefresh(combatWin);
}

void Window::clearWorldWin(){
	for(int i = 1 ; i < getmaxx(worldWin)-1 ; i++){
		for(int j = 1 ; j < getmaxy(worldWin)-1 ; j++){
			mvwprintw(worldWin, j,i," ");
		}
	}
	wrefresh(combatWin);
}

void Window::clearInfoWin(){
	for(int i = 1 ; i < 48 ; i++){
		for(int j = 1 ; j < 6 ; j++){
			mvwprintw(infoWin, j,i," ");
		}
	}
	wrefresh(infoWin);
}

void Window::activateBattle(int activePlayerMon){

	/*clearInfoWin();
	mvwprintw(infoWin, 1,1,"BATTLE!");
	wrefresh(infoWin);
	mvwaddch(worldWin, y, x, '!');
	wrefresh(worldWin);*/

	battleFloor = mvwinch(worldWin, worldWin->_maxx/2, worldWin->_maxy/2);

	clearWorldWin();
	mvwprintw(worldWin, getmaxy(worldWin)/2, getmaxx(worldWin)/2-4,"BATTLE!");
	wrefresh(worldWin);

	clickNext();

	srand(time(NULL));
	enemy = new ASCIIMonster(globalMonsters->getRandom(), rand()%(seed-(seed-5))+(1+(seed-(seed-2))));
	enemy->setHpToMax();

	clearInfoWin();
	mvwprintw(infoWin, 1, 1, "A wild ");
	mvwprintw(infoWin, 1, 8, enemy->getName().c_str());
	mvwprintw(infoWin, 2, 1, "appears!");
	mvwprintw(infoWin, 3, 1, "It is level %d!", enemy->getLvl());
	wrefresh(infoWin);

	clearCombatWin();
				
	combatWinActive = true;
	worldWinActive = false;

}

void Window::clickNext(){
	int accept = getch();
	while(accept != 'z' && accept != 'x'){
		accept = getch();
	}
}

void Window::refreshFix(){

	for(int i = x ; i < 29 ; i++){
			mvwprintw(worldWin, y,i, " ");	
		}
		wrefresh(worldWin);

}

void Window::moveMap(){

	
	battleFloor = mvwinch(worldWin, y, x);

	print(worldWin, x,y, Icon(WO_PLAYER));
	
	wrefresh(worldWin);
}

void Window::getKeyPress(){

	tmp=getch();

	switch(tmp){
        case KEY_LEFT:
             direction='l';
             break;
        case KEY_UP:
             direction='u';
            break;
        case KEY_DOWN:
             direction='d';
              break;
        case KEY_RIGHT:
            direction='r';
            break;
		case 'b':
			direction='b';
			break;
		case 'w':
			direction='w';
			break;
		case 'z':
			direction='z';
			break;
		case 'x':
			direction='x';
			break;
		default:
			direction='w';
			break;
	}

}

void Window::initColors(){

	start_color();
	init_color(COLOR_VIOLET, 0, 0, 0);
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_WHITE, COLOR_BLACK);
	init_pair(3, COLOR_RED, COLOR_BLACK);
	init_pair(4, COLOR_BLUE, COLOR_BLACK);
	init_pair(5, COLOR_GREEN, COLOR_BLACK);
	init_pair(6, COLOR_YELLOW, COLOR_BLACK);
	init_pair(7, COLOR_VIOLET, COLOR_BLACK);
	init_pair(8, COLOR_YELLOWER, COLOR_BLACK);
	init_pair(9, COLOR_REDDER, COLOR_BLACK);
	init_pair(10, COLOR_CYANER, COLOR_BLACK);
	init_pair(11, COLOR_CYAN, COLOR_BLACK);
	init_pair(12, COLOR_GREENER, COLOR_BLACK);
	init_pair(13, COLOR_BLUER, COLOR_BLACK);
	init_pair(14, COLOR_GRAY, COLOR_BLACK);
	init_pair(15, COLOR_BLACK, COLOR_WHITE);

}

void Window::info(){
	clearInfoWin();
	mvwprintw(infoWin, 1, 1, "Press 'b' to battle.");
	mvwprintw(infoWin, 2, 1, "Press 'm' to open menu. (not implemented yet)");
	wrefresh(infoWin);
}

void Window::runAway(){

		clearCombatWin();
		clearCombatMenu();
		clearInfoWin();
		wrefresh(combatWin);

		mvwprintw(infoWin, 1, 1, "You ran away...");
		wrefresh(infoWin);
		

		getKeyPress();

		clearInfoWin();
		wrefresh(infoWin);

		combatWinActive = false;
		worldWinActive = true;

		menuX = 2;
		menuY = 11;
		direction = NULL;

}

void Window::battleTurn(){
	clearInfoWin();
		mvwprintw(infoWin, 1, 1, playerSet->getMonster(activePlayerMon)->getName().c_str());
		mvwprintw(infoWin, 1, 1+strlen(playerSet->getMonster(activePlayerMon)->getName().c_str()), " used ");
		mvwprintw(infoWin, 1, 1+strlen(playerSet->getMonster(activePlayerMon)->getName().c_str())+6, playerSet->getMonster(activePlayerMon)->getMove(menuCY-10)->getName().c_str());
		wrefresh(infoWin);
		clickNext();

		if(playerSet->getMonster(activePlayerMon)->getMove(menuCY-10)->use()){
			enemy->receiveDmg(playerSet->getMonster(activePlayerMon)->getMove(menuCY-10)->getDmg());
			clearInfoWin();
			mvwprintw(infoWin, 1, 1, "Hit!");
			//suboptimal
			drawOpponent(enemy);
			wrefresh(infoWin);
			wrefresh(combatWin);
			clickNext();

		}else{
			if(playerSet->getMonster(activePlayerMon)->getMove(menuCY-10)->getPp() > 0){
				clearInfoWin();
				mvwprintw(infoWin, 1 ,1 ,"...but it missed!");
				wrefresh(infoWin);
			}else{
				clearInfoWin();
				mvwprintw(infoWin, 1 ,1 ,"Not enough PP!");
				wrefresh(infoWin);
			}

			clickNext();

		}

		//suboptimal
		drawMoves();
		wrefresh(combatWin);

		clearInfoWin();
		wrefresh(infoWin);
}

void Window::checkHp(){
	if(enemy->getHp()==0){
			playerSet->getMonster(activePlayerMon)->addExp(100*enemy->getLvl());
			clearInfoWin();
			mvwprintw(infoWin, 1, 1, "Enemy fainted.");
			wrefresh(infoWin);
			//suboptimal
			drawOpponent(enemy);
			wrefresh(combatWin);
			clickNext();

			clearInfoWin();
			mvwprintw(infoWin, 1, 1, "You got ");
			mvwprintw(infoWin, 1, 9, "%d", 100*enemy->getLvl());
			mvwprintw(infoWin, 1, 10+strlen(to_string(100*enemy->getLvl()).c_str()), "experience!");
			wrefresh(infoWin);
			clickNext();

			if(playerSet->getMonster(activePlayerMon)->checkLevelUp()){
				clearInfoWin();
				mvwprintw(infoWin, 1, 1, "Level up!");
				wrefresh(infoWin);
				clickNext();

			}

			clearInfoWin();
			clearCombatMenu();
			clearCombatWin();
			fightMenuActive = false;
			combatWinActive = false;
			mainMenuActive = true;
			worldWinActive = true;
		}
}

void Window::combat(){

	drawMoves();

	if(direction=='x'){
		clearCombatMenu();
		fightMenuActive = false;
		mainMenuActive = true;
		//direction = NULL;
		//suboptimal
		drawCombatMenu();

	}else if(direction=='u' && menuCY != 10){
		menuCY--;
	}else if(direction=='d' && menuCY != 9+moveCount){
		menuCY++;
	}else if(direction=='z'){
		direction=NULL;

		battleTurn();
		checkHp();
		
	}

	if(fightMenuActive){
		mvwprintw(combatWin, menuCY, 1, ">");
	}
	
}

bool Window::collisionDetect(char direction, int playerPositionX, int playerPositionY){
	if(direction == 'l'){
		if(worldMap->getBlock(playerPositionX-1, playerPositionY) == WO_WALL || worldMap->getBlock(playerPositionX-1, playerPositionY) == WO_DEEP_WATER || worldMap->getBlock(playerPositionX-1, playerPositionY) == WO_HILL || worldMap->getBlock(playerPositionX-1, playerPositionY) == WO_ROOF_ASCIICENTER || worldMap->getBlock(playerPositionX-1, playerPositionY) == WO_ROOF_GYM || worldMap->getBlock(playerPositionX-1, playerPositionY) == WO_ROOF_NORMAL_HOUSE || worldMap->getBlock(playerPositionX-1, playerPositionY) == WO_ROOF_SPAWN || worldMap->getBlock(playerPositionX-1, playerPositionY) == WO_ROOF_SHOP){
			return false;
		}else
			return true;
	}
	if(direction == 'r'){
		if(worldMap->getBlock(playerPositionX+1, playerPositionY) == WO_WALL || worldMap->getBlock(playerPositionX+1, playerPositionY) == WO_DEEP_WATER || worldMap->getBlock(playerPositionX+1, playerPositionY) == WO_HILL || worldMap->getBlock(playerPositionX+1, playerPositionY) == WO_ROOF_ASCIICENTER || worldMap->getBlock(playerPositionX+1, playerPositionY) == WO_ROOF_GYM || worldMap->getBlock(playerPositionX+1, playerPositionY) == WO_ROOF_NORMAL_HOUSE || worldMap->getBlock(playerPositionX+1, playerPositionY) == WO_ROOF_SPAWN || worldMap->getBlock(playerPositionX+1, playerPositionY) == WO_ROOF_SHOP){
			return false;
		}else
			return true;
	}
	if(direction == 'u'){
		if(worldMap->getBlock(playerPositionX, playerPositionY-1) == WO_WALL || worldMap->getBlock(playerPositionX, playerPositionY-1) == WO_DEEP_WATER || worldMap->getBlock(playerPositionX, playerPositionY-1) == WO_HILL || worldMap->getBlock(playerPositionX, playerPositionY-1) == WO_ROOF_ASCIICENTER || worldMap->getBlock(playerPositionX, playerPositionY-1) == WO_ROOF_GYM || worldMap->getBlock(playerPositionX, playerPositionY-1) == WO_ROOF_NORMAL_HOUSE || worldMap->getBlock(playerPositionX, playerPositionY-1) == WO_ROOF_SPAWN || worldMap->getBlock(playerPositionX, playerPositionY-1) == WO_ROOF_SHOP){
			return false;
		}else
			return true;
	}
	if(direction == 'd'){
		if(worldMap->getBlock(playerPositionX, playerPositionY+1) == WO_WALL || worldMap->getBlock(playerPositionX, playerPositionY+1) == WO_DEEP_WATER || worldMap->getBlock(playerPositionX, playerPositionY+1) == WO_HILL || worldMap->getBlock(playerPositionX, playerPositionY+1) == WO_ROOF_ASCIICENTER || worldMap->getBlock(playerPositionX, playerPositionY+1) == WO_ROOF_GYM || worldMap->getBlock(playerPositionX, playerPositionY+1) == WO_ROOF_NORMAL_HOUSE || worldMap->getBlock(playerPositionX, playerPositionY+1) == WO_ROOF_SPAWN || worldMap->getBlock(playerPositionX, playerPositionY+1) == WO_ROOF_SHOP){
			return false;
		}else
			return true;
	}
}