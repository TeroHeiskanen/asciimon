#include "Enumerations.h"
#include <string>
#include <map>

using namespace std;

#ifndef MOVE_H
#define MOVE_H

class Move{
private:
	string name;
	int dmg;
	int pp;
	int accuracy;
	int lvlReq;
	MONSTERTYPE type;

public:
	Move();
	Move(string name, MONSTERTYPE type, int lvlReq, int dmg, int pp, int accuracy);
	string getName();
	MONSTERTYPE getType();
	int getLvlReq();
	int getDmg();
	int getPp();
	int getAccuracy();
	bool use();
	void print();
};

class Moves{
private:
	map<int,Move> water, fire, grass, electric, normal, psychic, fighting, ground, ice, poison, bug;
	void readMoves();
	MONSTERTYPE getType(string type);
	void addMove(Move m);
public:
	Moves();
	Move* getMove(MONSTERTYPE type, int lvl);
	Move* getStartingMove(MONSTERTYPE type, int lvl);
	void print();
};

#endif