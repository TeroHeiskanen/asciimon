﻿/* 
*WORLD SIS�LT�� KAIKEN MAAILMAN LUONTIIN LIITTYV�N. 
*/
  
  
#include "World.h" 
#include <time.h> 
#include <windows.h> 
#include <random> 
#include <iostream> 
#include <fstream> 
#include <math.h> 
#include <algorithm> 
#include <map> 
#include <string> 
#include <mutex> 
  
using namespace std; 
  
void writePixel(ofstream &out, WORLDOBJ pixel); 
  
vector<vector<vector<int>>> threadVectors; 
  
mutex mtx; 
  
//VAKIOKONSTRUKTORI WORLD LUOKALLE, TARVITAAN VAIN MUISTINVARAUKSEEN, EI K�YTET� MUUTOIN 
  
World::World(){ 
} 
  
//DESTRUKTORI WORLD LUOKALLE, POISTAA KAIKKIEN POINTTEREIDEN SIS�LL�N 
  
World::~World(){ 
    delete pn; 
    for(int i = 0 ; i < size ; i++){ 
        delete map[i]; 
    } 
    delete map; 
    heights.erase(heights.begin(),heights.end()); 
} 
  
//KONSTRUKTORI WORLD LUOKALLE, JOLLE VOI SY�TT�� KARTAN KOON 
  
World::World(int size){ 
    this->size = size; 
  
    //LUODAAN MATRIISI WORLDBLOCK LUOKAN OLIOISTA, SIS�LT�� KARTAN 
  
    map = new WorldBlock*[size]; 
    for(int i = 0 ; i < size ; i++){ 
        map[i] = new WorldBlock[size]; 
    } 
      
    //LUODAAN PERLINNOISE LUOKAN OLIO, M��RITET��N AMPLITUDIKSI 128, JOLLOIN SAADAAN ARVOJA V�LILT� -128 JA 128 
  
    pn = new PerlinNoise(); 
    pn->SetAmplitude(128); 
  
    //AJETAAN KARTANLUONTI 
  
    generate(rand()); 
} 
  
//SIS�LT�� KAIKEN KARTANLUONNIN, PARAMETRIN� VOIDAAN SY�TT�� SEED 
//SAMALLA SEEDILL� TULEE AINA SAMAN N�K�ISI� KARTTOJA 
  
void World::generate(int randomseed){ 
    int selection, oct; 
    bool generateRoads; 
    double freq, pers; 
  
    //KYSYT��N JOS K�YTT�J� HALUAA M��RITT�� OMAN SEEDIN, MUUTOIN K�YTET��N 
    //PARAMETRIN� SAATUA SEEDI� 
  
    cout<<"Give seed, zero (0) for random seed."<<endl<<"Selection: "; 
    cin>>randomseed; 
  
    if(randomseed == 0){ 
        srand(time(NULL)); 
    }else{ 
        srand(randomseed); 
    } 
  
    system("cls"); 
  
    //M��RITELL��N PARAMETRIT PERLINNOISELLE KARTAN KOON MUKAAN 
  
    switch(size){ 
    case 512: 
        freq = 0.04; 
        pers = 0.5; 
        oct = 4; 
        break; 
    case 1024: 
        freq = 0.02; 
        pers = 0.5; 
        oct = 4; 
        break; 
    case 2048: 
        freq = 0.018; 
        pers = 0.5; 
        oct = 4; 
        break; 
    case 4096: 
        freq = 0.014; 
        pers = 0.5; 
        oct = 4; 
        break; 
    default: 
        cout<<"Invalid size!"<<endl; 
        return; 
    } 
      
    //KYSYT��N JOS K�YTT�J� HALUAA LUODA TIET 
  
    cout<<"Do you want to generate roads?"<<endl<<"1: Yes"<<endl<<"2: No"<<endl<<"Selection: "; 
    cin>>selection; 
  
    system("cls"); 
  
    if(selection == 1){ 
        generateRoads = true; 
    } 
    else{ 
        generateRoads = false; 
    } 
  
    pn->SetRandomSeed(randomseed); 
      
    //LUODAAN MAASTO PERLINNOISEN PARAMETREILL� 
  
    generateLand(pers,freq,oct); 
  
    //LUODAAN KASVUSTO "KORKEUSKARTTA" 
  
    switch(size){ 
        case 512: 
            generateForestAndGrass(50,0.5,0.1,2); 
            break; 
        case 1024: 
            generateForestAndGrass(50,0.5,0.05,2); 
            break; 
        case 2048: 
            generateForestAndGrass(50,0.4,0.1,2); 
            break; 
        case 4096: 
            generateForestAndGrass(50,0.3,0.1,2); 
            break; 
        default: 
            cout<<"Invalid map size!"<<endl; 
            return; 
        }    
  
    //ETSIT��N PAIKKA MIST� PELI ALKAA JA LUODAAN SINNE ALOITUSKAUPUNKI 
  
    generateSpawn(); 
  
    //LUODAAN MAX 200 KAUPUNKIA, TODELLISUUDESSA NIIT� TULEE SIIS NIIN PALJON 
    //KUN VAIN KARTTAAN MAHTUU 
  
    generateCities(200); 
      
    //LIS�T��N KASVUSTO KARTTAAN 
  
    mergeMapWithForestAndGrass(); 
  
    double start = clock(); 
  
    //LUODAAN TIET KAUPUNKIEN V�LILLE 
  
    if(generateRoads){ 
        //LUODAAN TIET JOKAISESTA KAUPUNGISTA L�HIMP�N� OLEVAAN KAUPUNKIIN 
        //T�M�N J�LKEEN TIEVERKKO EI OLE VIEL� KATTAVA 
        connectCities(); 
      
        //TARKASTETAAN ONKO TIEVERKKO KATTAVA, JOS EI NIIN TARKASTETAAN MISS� ON 
        //L�HIN EI TIEVERKKOON KUULUVA KAUPUNKI JA VEDET��N SINNE TIE L�HIMM�ST� 
        //KAUPUNGISTA. T�T� TOISTETAAN KUNNES TIEVERKKO KATTAA KAIKKI KAUPUNGIT 
        while(connectedCities.size() != cities.size()){ 
            connectedCities.erase(connectedCities.begin(),connectedCities.end()); 
            findConnected(&cities[0]); 
            connectNearestUnconnected();         
        } 
        /*for(int i = 0 ; i < roadThreads.size() ; i++){ 
            roadThreads[i].join(); 
        }*/
    } 
    //TYHJENNET��N VECTORIT 
    connectedCities.erase(connectedCities.begin(),connectedCities.end());    
    roadThreads.erase(roadThreads.begin(),roadThreads.end()); 
  
    cout<<clock()-start<<endl; 
    //LUODAAN BMP KUVA KARTASTA 
    makeMapBmp("worldmap.bmp"); 
} 
  
//LUO KORKEUSKARTAN, JONKA PERUSTEELLA METS�T JA RUOHIKKO LIS�T��N KARTALLE 
void World::generateForestAndGrass(int percentage, double persistence, double frequency, int octaves){ 
    //M��R�T��N PERLINNOISELLE PARAMETRIT 
    pn->SetPersistence(persistence); 
    pn->SetFrequency(frequency); 
    pn->SetOctaves(octaves); 
  
    vector<int> heights; 
    int calculatedHeight; 
  
    //LUODAAN P��KARTAN KANSSA SAMANKOKOINEN KORKEUSKARTTA 
    forestMap = vector<vector<int>>(size,vector<int>(size)); 
  
    //LIS�T��N JOKAISELLE KORKEUSKARTAN PISTEELLE KORKEUS PERLINNOISELLA 
    //LIS�T��N JOKAINEN KORKEUS MY�S HEIGHTS VECTORIIN, JONKA PERUSTEELLA 
    //P��TET��N MIK� KORKEUSV�LI VASTAA METS�� JA MIK� RUOHIKKOA 
    for(int i = 0 ; i < size ; i++){ 
        for(int j = 0 ; j < size ; j++){ 
            calculatedHeight = pn->GetHeight(i,j); 
            calculatedHeight = (heightmap[i][j]+calculatedHeight)/2; 
            forestMap[i][j] = calculatedHeight; 
            heights.push_back(calculatedHeight); 
        } 
    } 
  
    //J�RJESTET��N KORKEUDET PIENIMM�ST� SUURIMPAAN 
    sort(heights.begin(),heights.end()); 
  
    //P��TET��N MIK� KORKEUSV�LI VASTAA RUOHIKKOA JA MIK� METS�� 
    forestLevel = heights[float(heights.size()*percentage/100)]; 
    grassLevel = heights[float(heights.size()*(percentage+20)/100)]; 
  
    return; 
} 
  
//LIS�� RUOHIKON KARTTAAN 
void World::mergeMapWithForestAndGrass(){ 
    for(int i = 0 ; i < size ; i++){ 
        for(int j = 0 ; j < size ; j++){ 
            //LIS�� METS��/RUOHOA VAIN JOS PAIKKA ON SOPIVA JA JOS METS�N/RUOHIKON 
            //KORKEUS ON OIKEA 
            if(isAllowedForForest(i,j) && forestMap[i][j] < forestLevel){ 
                map[i][j] = WorldBlock(WO_FOREST); 
            } 
            if(isAllowedForGrass(i,j) && forestMap[i][j] < grassLevel){ 
				if(rand()%100 > 49){
                map[i][j] = WorldBlock(WO_GRASS); 
				}else{
				map[i][j] = WorldBlock(WO_GRASS_ALT); 
				}
            } 
        } 
    } 
} 
  
//TARKASTAA ONKO PAIKKA SOPIVA METS�LLE 
bool World::isAllowedForForest(int x, int y){ 
    if(x > 1 && x < size-2 && y > 1 && y < size-2){ 
        for(int i = -2 ; i < 3 ; i++){ 
            for(int j = -2 ; j < 3 ; j++){ 
                switch(map[x+i][y+j].getBlock()){ 
                case WO_LAND: 
                    break; 
                case WO_LAND_ALT: 
                    break; 
                case WO_HI_LAND: 
                    break; 
                case WO_HI_LAND_ALT: 
                    break; 
                case WO_GRASS: 
                    break; 
                case WO_FOREST: 
                    break; 
                default: 
                    return false; 
                } 
            } 
        } 
        return true; 
    } 
    return false; 
} 
//TARKASTAA ONKO PAIKKA SOPIVA RUOHOLLE 
bool World::isAllowedForGrass(int x, int y){ 
    if(x > 0 && x < size-1 && y > 0 && y < size-1){ 
        switch(map[x][y].getBlock()){ 
        case WO_LAND: 
            break; 
        case WO_LAND_ALT: 
            break; 
        case WO_HI_LAND: 
            break; 
        case WO_HI_LAND_ALT: 
            break; 
        default: 
            return false; 
        } 
        return true; 
    } 
    return false; 
} 
//LUO MAASTON  
void World::generateLand(double persistence, double frequency, int octaves){ 
    //M��R�T��N PERLINNOISELLE PARAMETRIT 
    pn->SetPersistence(persistence); 
    pn->SetFrequency(frequency); 
    pn->SetOctaves(octaves); 
  
    int subSize; 
      
    //HAKEE TIEDON PROSESSORIN S�IKEIST�     
    int maxThreads = int(thread::hardware_concurrency()); 
    vector<thread> threads(maxThreads); 
  
    //LASKEE, KUINKA "KORKEITA" SUIKALEITA MIK�KIN S�IE LASKEE 
    subSize = size/maxThreads; 
  
    //VARAA MUISTIN VECTOREILLE 
    heights = vector<int>(size*size); 
    heightmap = vector<vector<int>>(size,vector<int>(size)); 
    threadVectors = vector<vector<vector<int>>>(maxThreads,vector<vector<int>>(subSize,vector<int>(size))); 
  
    cout<<"Generating land!"<<endl; 
  
    //LUO S�IKEET KARTAN LUONTIIN 
    //PARAMETRIN� ANNETAAN TIEDOT MIST� PISTEEST� KARTTAA L�HDET��N LUOMAAN 
    //JA KUINKA KORKEA SUIKALE SIT� LUODAAN 
    for(int i = 0 ; i < maxThreads ; i++){ 
        threads[i] = thread(&World::mapThread, this, &threadVectors[i], subSize*i, (subSize*(i+1))-1); 
    } 
  
    //ODOTETAAN ETT� KAIKKI S�IKEET OVAT VALMIITA 
    //JA LIS�T��N NE V�LIAIKAISEEN KORKEUSKARTTAAN 
    for(int i = 0, k = 0; i < maxThreads && k < size; i++){ 
        threads[i].join(); 
        for(int j = 0 ; j < subSize ; j++, k++){ 
            heightmap[k].insert(heightmap[k].begin(), threadVectors[i][j].begin(), threadVectors[i][j].end()); 
        } 
    } 
  
    //LIS�T��N KORKEUDET MY�S TOISEEN VECTORIIN, JONKA AVULLA 
    //M��RITET��N MIK� KORKEUS VASTAA MIT�KIN (ESIM VETT�) 
    for(int i = 0, j = 0 ; i < size ; i++){ 
        for(int k = 0 ; k < size ; k++, j++){ 
            heights[j] = heightmap[i][k]; 
        } 
    } 
  
    cout<<"Calculating height levels!"<<endl; 
  
    //LASKEE MIK� KORKEUSV�LI VASTAA MIT�KIN 
    calculateLevels();   
  
    //LUO KARTAN KORKEUSKARTAN JA M��R�TTYJEN KORKEUSV�LIEN AVULLA 
    for(int i = 0 ; i < size ; i++){ 
        for(int j = 0 ; j < size ; j++){ 
            map[i][j] = WorldBlock(getTileType(heightmap[i][j])); 
        } 
    }        
  
    cout<<"Smoothing out the map!"<<endl; 
  
    //PEHMENT�� KARTAN ROSOISUUTTA 
    for(int i = 0 ; i < 6 ; i++){ 
        smoothenMap(); 
    } 
      
    //TYHJENNET��N VECTORIT 
    heights.erase(heights.begin(),heights.end()); 
    threadVectors.erase(threadVectors.begin(),threadVectors.end()); 
    threads.erase(threads.begin(),threads.end()); 
} 
  
//HAKEE SOPIVAN PAIKAN ALOITUSKAUPUNGILLE JA LUO SEN 
void World::generateSpawn(){ 
    cout<<"Generating spawn!"<<endl; 
    int center = size/2; 
    int spawnSquare = size/4;    
    int count = 0; 
  
    do{ 
        //HAETAAN PAIKKA ALOITUSKAUPUNGILLE 
        getSpawn(center+rand()%spawnSquare-(spawnSquare/2),center+rand()%spawnSquare-(spawnSquare/2),0); 
  
        //KATSOTAAN ONKO ALOITUSPAIKKA SOPIVALLA ALUEELLA 
        //JOS KYLL�, LUODAAN UUSI SPAWNCITY OLIO JA LIS�T��N 
        //SE KAUPUNKI VECTORIIN 
        if(spawnX > 63 && spawnX < size-64 && spawnY > 63 && spawnY < size-64){ 
            spawnCity = SpawnCity(spawnX,spawnY); 
            cities.push_back(spawnCity); 
            return; 
        } 
        count++; 
        //AINA SOPIVAA ALUETTA EI L�YDY, JOTEN YRITET��N 
        //HAKEA 10000 KERTAA, JOS EI L�YDY NIIN LUOVUTETAAN 
    }while(count < 9999); 
  
    cout<<"Couldn't find space for spawn!"<<endl; 
} 
  
//HAKEE SOPIVAN PAIKAN ALOITUSKAUPUNGILLE, TOIMII REKURSIIVISESTI 
//PARAMETREIN� ANNETAAN PISTE MISS� OLLAAN JA MIHIN SUUNTAAN OLLAAN 
//MENOSSA. ENSIMM�ISELL� KUTSUKERRALLA ANNETAAN SUUNNAKSI 0, JOTEN 
//TIEDET��N ETT� ON KYSEESS� ENSIMM�INEN KUTSU 
void World::getSpawn(int x, int y, int orient){ 
  
    if(x > 0 && x < size-1 && y > 0 && y < size-1){ 
        WORLDOBJ current = map[x][y].getBlock(); 
        //JOS ENSIMM�INEN KUTSUKERTA, ARVOTAAN SUUNTA JONNE L�HDET��N 
        //MAHDOLLISET SUUNNAT OVAT KOMPASSIN SUUNTIA 
        if(orient == 0){ 
            orient = rand()%8+1;             
        } 
        //TARKASTETAAN ONKO 19*19 ALUE SOPIVA KAUPUNGILLE 
        //JOS KYLL�, ASETETAAN ALOITUSKAUPUNGIN PARAMETRIT JA 
        //POISTUTAAN 
        if(isPassableArea(x,y,9)){ 
            spawnX = x; 
            spawnY = y; 
            return; 
        } 
        //JOS PAIKKA EI OLLUT SOPIVA, SIIRRYT��N SUUNNAN MUKAAN 
        //SEURAAVAAN PAIKKAAN JA AJETAAN SAMA UUSIKSI 
        switch(orient){ 
        case 1: 
            getSpawn(x+1,y,orient); 
            return; 
        case 2: 
            getSpawn(x-1,y,orient); 
            return; 
        case 3: 
            getSpawn(x,y+1,orient); 
            return; 
        case 4: 
            getSpawn(x,y-1,orient); 
            return; 
        case 5: 
            getSpawn(x+1,y+1,orient); 
            return; 
        case 6: 
            getSpawn(x+1,y-1,orient); 
            return; 
        case 7: 
            getSpawn(x-1,y+1,orient); 
            return; 
        case 8: 
            getSpawn(x-1,y-1,orient); 
            return; 
        default: 
            return; 
        } 
    } 
    //JOS JOSTAIN SYYST� SAADUT PARAMETRIT OSOITTIVAT 
    //KARTAN ULKOPUOLELLE, ANNETAAN KOORDINAATEIKSI 
    //-1 JA POISTUTAAN 
    spawnX = -1; 
    spawnY = -1; 
    return; 
} 


//PALAUTTAA ALOITUSPAIKAN X-KOORDINAATIN
int World::getSpawnX(){
	return spawnX;
}

//PALAUTTAA ALOITUSPAIKAN Y-KOORDINAATIN
int World::getSpawnY(){
	return spawnY;
}
  
//VET�� TIEN JOKAISESTA KAUPUNGISTA L�HIMP��N KAUPUNKIIN, T�M�N J�LKEEN TIEVERKKO EI OLE VIEL� KATTAVA 
void World::connectCities(){ 
    cout<<"Connecting cities!"<<endl; 
    int distanceToNearest, distanceToCurrent; 
    City *temp; 
    City *closest; 
  
    //HAKEE JOKAISELLE KAUPUNGILLE L�HIMM�N KAUPUNGIN 
    //MERKATAAN MOLEMMILLE KAUPUNGEILLE LINKIKSI TOISENSA 
    for(int i = 0 ; i < cities.size() ; i++){ 
        temp = &cities[i]; 
        closest = nearestCity(temp); 
        if(!temp->isConnected(closest) && !closest->isConnected(temp)){ 
            int x1 = temp->getRoadX(), y1 = temp->getRoadY(); 
            int x2 = closest->getRoadX(), y2 = closest->getRoadY(); 
            generateRoad(Coordinate(x1,y1),Coordinate(x2,y2),false); 
            temp->addConnection(closest); 
            closest->addConnection(temp); 
        } 
    } 
} 
  
//HAKEE TIETYST� KAUPUNGISTA KAIKKI SIIHEN LIITETYT KAUPUNGIT 
//ETSII MIK� JO LINKITETTY KAUPUNKI ON L�HINN� EI LIITETTY� KAUPUNKIA JA VET�� 
//TIEN NIIDEN V�LILLE 
void World::connectNearestUnconnected(){ 
    City *bestLink = NULL, *nearestUnconnected = NULL; 
    City *tempBest, *tempNearest; 
    int nearestDistance, tempDistance; 
    int x1, y1, x2, y2; 
  
    //HAKEE L�HIMM�N EI LINKITETYN 
    for(int i = 0 ; i < connectedCities.size() ; i++){ 
        tempBest = connectedCities[i]; 
        tempNearest = nearestUnconnectedCity(tempBest); 
  
        if(tempNearest == NULL){ 
            return; 
        } 
        x1 = tempBest->getRoadX(); 
        y1 = tempBest->getRoadY(); 
        x2 = tempNearest->getRoadX(); 
        y2 = tempNearest->getRoadY(); 
          
        tempDistance = distance(x1,y1,x2,y2); 
  
        if(bestLink == NULL || tempDistance < nearestDistance){ 
            nearestDistance = tempDistance; 
            bestLink = tempBest; 
            nearestUnconnected = tempNearest; 
        } 
    } 
  
    //VEDET��N TIE PARHAIMMALLE LINKKIV�LILLE 
    //MERKATAAN MOLEMMILLE KAUPUNGEILLE LINKIKSI TOISENSA 
    x1 = bestLink->getRoadX(); 
    y1 = bestLink->getRoadY(); 
    x2 = nearestUnconnected->getRoadX(); 
    y2 = nearestUnconnected->getRoadY(); 
  
    //roadThreads.push_back(thread(&World::generateRoad,this,Coordinate(x1,y1),Coordinate(x2,y2),false)); 
    generateRoad(Coordinate(x1,y1),Coordinate(x2,y2),false); 
    bestLink->addConnection(nearestUnconnected); 
    nearestUnconnected->addConnection(bestLink); 
} 
  
//HAKEE L�HIMM�N EI LINKITETYN KAUPUNGIN 
City* World::nearestUnconnectedCity(City *c){ 
    int x = c->getRoadX(); 
    int y = c->getRoadY(); 
    int closestDistance; 
    City *temp, *closestCity = NULL; 
  
    for(int i = 0 ; i < cities.size() ; i++){ 
        temp = &cities[i]; 
        if(*temp != *c && !isConnected(temp)){ 
            if(closestCity == NULL  || closestDistance > distance(x,y,temp->getRoadX(),temp->getRoadY())){ 
                closestDistance = distance(x,y,temp->getRoadX(),temp->getRoadY()); 
                closestCity = temp; 
            } 
        } 
    } 
    return closestCity; 
} 
  
//KARTOITTAA JOSTAIN KAUPUNGISTA ALKAEN KAIKKI SIIHEN VERKKOON KUULUVAT KAUPUNGIT 
void World::findConnected(City *c){  
    City *current = c, *temp; 
  
    addToConnected(c); 
  
    for(int i = 0 ; i < cities.size() ; i++){ 
        temp = &cities[i]; 
        if(c->isConnected(temp)){ 
            if(addToConnected(temp)){ 
                findConnected(temp); 
            } 
        } 
    } 
    return; 
} 
  
//TARKASTAA KAUPUNGIN LINKEIST� ONKO KYSEISELL� KAUPUNGILLA JO YHTEYS SIIHEN 
bool World::isConnected(City *c){ 
    for(int i = 0 ; i < connectedCities.size() ; i++){ 
        if(*c == *connectedCities[i]){ 
            return true; 
        } 
    } 
    return false; 
} 
  
//LIS�� KAUPUNGIN LINKITETTYJEN LISTAAN 
bool World::addToConnected(City *c){ 
    for(int i = 0 ; i < connectedCities.size() ; i++){ 
        if(*c == *connectedCities[i]){ 
            return false; 
        } 
    } 
    connectedCities.push_back(c); 
    return true; 
} 
  
//HAKEE L�HIMM�N KAUPUNGIN 
City* World::nearestCity(City *c){ 
    int x = c->getRoadX(); 
    int y = c->getRoadY(); 
    int closestDistance; 
    City *temp, *closestCity = NULL; 
  
    for(int i = 0 ; i < cities.size() ; i++){ 
        temp = &cities[i]; 
        if(*temp != *c){ 
            if(closestCity == NULL  || closestDistance > distance(x,y,temp->getRoadX(),temp->getRoadY())){ 
                closestDistance = distance(x,y,temp->getRoadX(),temp->getRoadY()); 
                closestCity = temp; 
            } 
        } 
    } 
      
    return closestCity; 
} 
  
//YRITT�� LUODA HALUTUN M��R�N KAUPUNKEJA, ONNISTUMINEN RIIPPUU KARTAN MAASTOSTA 
void World::generateCities(int count){ 
    cout<<"Generating cities!"<<endl; 
    int curX, curY; 
    int neededArea; 
    int randCity; 
    bool cont, stopTrying = false; 
    City temp; 
  
    //ETSII SATUNNAISESTI PAIKAN HALUTULLE M��R�LLE KAUPUNKEJA JA LIS�� NE VECTORIIN 
    //HAKEE JOKAISELLE KAUPUNGILLE MAX 9999 ERI SATUNNAISTA ALUETTA, JOS MIK��N NIIST� 
    //EI OLE SOPIVA, ALGORITMI LUOVUTTAA 
    for(int i = 0, tries ; i < count && !stopTrying ; i++){ 
        tries = 0; 
        randCity = rand()%3; 
        switch(randCity){ 
            case 0: 
                neededArea = 11; 
                break; 
            case 1: 
                neededArea = 16; 
                break; 
            case 2: 
                neededArea = 21; 
                break; 
        } 
        do{ 
            tries++; 
            cont = true; 
            curX = rand()%size; 
            curY = rand()%size; 
              
            if(isPassableArea(curX,curY,neededArea) && !citiesWithinDistance(curX,curY,size/6)){ 
                cont = false; 
            } 
            if(tries > 9999){ 
                stopTrying = true; 
            } 
        }while(cont && !stopTrying); 
        if(!stopTrying){ 
            switch(randCity){ 
                case 0: 
                    cities.push_back(SmallCity(curX,curY)); 
                    break; 
                case 1: 
                    cities.push_back(MediumCity(curX,curY)); 
                    break; 
                case 2: 
                    cities.push_back(LargeCity(curX,curY)); 
                    break; 
            } 
        } 
    } 
  
    //RAKENTAA KAIKKI CITIES VECTORIN SIS�LT�M�T KAUPUNGIT 
    for(int i = 0 ; i < cities.size() ; i++){ 
        generateCity(&cities[i]); 
    } 
} 
  
//RAKENTAA KAUPUNGIN JOKAISEN TALON JA VET�� TIET NIIST� KAUPUNGIN KESKIKOHTAAN 
void World::generateCity(City *city){    
    int fromX, fromY, toX, toY; 
    for(int i = 0 ; i < city->getBuildingCount() ; i++){ 
        generateBuilding(city->getBuilding(i)); 
    } 
    for(int i = 0 ; i < city->getBuildingCount() ; i++){ 
        fromX = city->getRoadX(); 
        fromY = city->getRoadY(); 
        toX = city->getBuilding(i)->getLocationX(); 
        toY = city->getBuilding(i)->getLocationY()+1; 
        generateRoad(Coordinate(fromX,fromY),Coordinate(toX,toY), true); 
    }    
} 
  
//TARKASTAA ONKO ALUEELLA KAUPUNKI 
bool World::citiesWithinDistance(int x, int y, int d){ 
    for(int i = 0 ; i < cities.size() ; i++){ 
        if(distance(x,y,cities[i].getRoadX(),cities[i].getRoadY()) < d){ 
            return true; 
        } 
    } 
    return false; 
} 
  
//PALAUTTAA KAHDEN PISTEEN V�LISEN ET�ISYYDEN 
int World::distance(int x1, int y1, int x2, int y2){ 
    return sqrt(pow(x1-x2,2)+pow(y1-y2,2)); 
} 
  
//LUO TIEN KAHDEN PISTEEN V�LILLE K�YTT�EN A*-ALGORITMIA 
void World::generateRoad(Coordinate cStart, Coordinate cGoal, bool cityRoad){ 
    int startX = cStart.x, startY = cStart.y; 
    int goalX = cGoal.x, goalY = cGoal.y; 
  
    if(startX == goalX && startY == goalY){ 
        return; 
    } 
    std::map<Coordinate,RoadNode> nodes; 
    vector<NodePointer> openNodes; 
    RoadNode *temp = new RoadNode(),*parent; 
  
    int x, y, lastX, lastY; 
    bool roadFound = false; 
    double start,stop; 
  
    //ASETTAA ENSIMM�ISEN NODEN TIEDOT 
  
    temp->closed = true; 
    temp->open = false; 
    temp->x = startX; 
    temp->y = startY; 
    temp->parent = NULL; 
  
    temp->G = 0; 
    temp->H = calculateHeuristic(temp,goalX,goalY); 
    temp->F = temp->G+temp->H; 
  
    nodes[Coordinate(startX,startY)] = *temp; 
      
    x = startX; 
    y = startY; 
    lastX = x; 
    lastY = y; 
    //K�Y L�PI NODEJA NIIN KAUAN KUNNES VAPAITA NODEJA EI EN�� OLE TAI KUNNES P��TEPISTE ON L�YDETTY 
    do{ 
        parent = &nodes[Coordinate(x,y)]; 
          
        //LIS�� JOKAISEN YMP�RILL� OLEVAN NODEN OPEN LISTIIN, JOS SE EI SIELL� JO OLE TAI JOS SE ON 
        //SIIHEN SOVELTUVA 
        temp = &nodes[Coordinate(x+1,y)]; 
        if(formatNode(temp,parent,x+1,y,goalX,goalY)){ 
            openNodes.push_back(NodePointer(temp)); 
            if(!temp->closed){ 
                checkNode(temp,parent); 
            } 
        } 
        temp = &nodes[Coordinate(x-1,y)]; 
        if(formatNode(temp,parent,x-1,y,goalX,goalY)){ 
            openNodes.push_back(NodePointer(temp)); 
            if(!temp->closed){ 
                checkNode(temp,parent); 
            } 
        } 
        temp = &nodes[Coordinate(x,y+1)]; 
        if(formatNode(temp,parent,x,y+1,goalX,goalY)){ 
            openNodes.push_back(NodePointer(temp)); 
            if(!temp->closed){ 
                checkNode(temp,parent); 
            } 
        } 
        temp = &nodes[Coordinate(x,y-1)]; 
        if(formatNode(temp,parent,x,y-1,goalX,goalY)){ 
            openNodes.push_back(NodePointer(temp)); 
            if(!temp->closed){ 
                checkNode(temp,parent); 
            } 
        } 
        if(!cityRoad){ 
            temp = &nodes[Coordinate(x+1,y+1)]; 
            if(formatNode(temp,parent,x+1,y+1,goalX,goalY)){ 
                openNodes.push_back(NodePointer(temp)); 
                if(!temp->closed){ 
                    checkNode(temp,parent); 
                } 
            } 
            temp = &nodes[Coordinate(x-1,y-1)]; 
            if(formatNode(temp,parent,x-1,y-1,goalX,goalY)){ 
                openNodes.push_back(NodePointer(temp)); 
                if(!temp->closed){ 
                    checkNode(temp,parent); 
                } 
            } 
            temp = &nodes[Coordinate(x-1,y+1)]; 
            if(formatNode(temp,parent,x-1,y+1,goalX,goalY)){ 
                openNodes.push_back(NodePointer(temp)); 
                if(!temp->closed){ 
                    checkNode(temp,parent); 
                } 
            } 
            temp = &nodes[Coordinate(x+1,y-1)]; 
            if(formatNode(temp,parent,x+1,y-1,goalX,goalY)){ 
                openNodes.push_back(NodePointer(temp)); 
                if(!temp->closed){ 
                    checkNode(temp,parent); 
                } 
            } 
        } 
          
        //TARKASTAA ONKO OPEN LISTIN KOKO SUUREMPI KUIN NOLLA, JOS ON NIIN LAITETAAN 
        //NODET HINNAN MUKAAN J�RJESTYKSEEN JA VALITAAN NIIST� HALVIN NODE SEURAAVAKSI 
        //K�SITTELYYN 
        if(openNodes.size() > 0){ 
            sort(openNodes.begin(),openNodes.end()); 
            temp = openNodes[0].ptr; 
            openNodes.erase(openNodes.begin());  
  
            temp->closed = true; 
            lastX = x; 
            lastY = y; 
            x = temp->x; 
            y = temp->y;          
        } 
  
        //ALGORITMI PYS�HTYY KUN MAALI ON L�YDETTY 
        if(x == goalX && y == goalY) 
            break; 
    }while(openNodes.size() > 0); 
  
    //VALITAAN TEMP NODEKSI MAALIN NODE JA ALETAAN VET�M��N SIIT� TIET� 
    //KOHTI ALOITUSPISTETT� SEURAAMALLA JOKAISEN NODEN VANHEMPIA 
    temp = &nodes[Coordinate(goalX,goalY)]; 
      
    if(!temp->closed && !temp->open){ 
        return; 
    } 
      
    x = temp->x; 
    y = temp->y; 
      
    //TEKEE TIEN TAI SILLAN X,Y PISTEESEEN JA SIIRTYY X,Y PISTEEN VANHEMPAAN 
    //T�T� JATKETAAN KUNNES X,Y ON ALOITUSPISTE 
    while(x != startX || y != startY){       
        if(cityRoad){ 
            map[x][y] = WorldBlock(WO_ROAD); 
        } 
        else{ 
            if(map[x][y].getBlock() == WO_WATER || map[x][y].getBlock() == WO_DEEP_WATER){ 
                map[x][y] = WorldBlock(WO_BRIDGE); 
                if(validForRoad(x+1,y)){ 
                    map[x+1][y] = WorldBlock(WO_BRIDGE); 
                } 
                if(validForRoad(x-1,y)){ 
                    map[x-1][y] = WorldBlock(WO_BRIDGE); 
                } 
                if(validForRoad(x,y+1)){ 
                    map[x][y+1] = WorldBlock(WO_BRIDGE); 
                } 
                if(validForRoad(x,y-1)){ 
                    map[x][y-1] = WorldBlock(WO_BRIDGE); 
                } 
            } 
            else{ 
                map[x][y] = WorldBlock(WO_LARGE_ROAD); 
                if(validForRoad(x+1,y)){ 
                    map[x+1][y] = WorldBlock(WO_LARGE_ROAD); 
                } 
                if(validForRoad(x-1,y)){ 
                    map[x-1][y] = WorldBlock(WO_LARGE_ROAD); 
                } 
                if(validForRoad(x,y+1)){ 
                    map[x][y+1] = WorldBlock(WO_LARGE_ROAD); 
                } 
                if(validForRoad(x,y-1)){ 
                    map[x][y-1] = WorldBlock(WO_LARGE_ROAD); 
                } 
            } 
        } 
        temp = temp->parent; 
        x = temp->x; 
        y = temp->y; 
    } 
  
    if(x == startX && y == startY){ 
        map[x][y] = WorldBlock(WO_ROAD); 
    } 
} 
  
//TARKASTAA ONKO MAASTO SOPIVA TIELLE 
bool World::validForRoad(int x, int y){ 
    switch(map[x][y].getBlock()){ 
    case WO_LAND: 
        return true; 
    case WO_LAND_ALT: 
        return true; 
    case WO_HI_LAND: 
        return true; 
    case WO_HI_LAND_ALT: 
        return true; 
    case WO_WATER: 
        return true; 
    case WO_DEEP_WATER: 
        return true; 
    case WO_SAND: 
        return true; 
    case WO_FOREST: 
        return true; 
    case WO_GRASS: 
        return true; 
    default: 
        return false; 
    } 
} 
  
//ARVIOI HINNAN TIETYST� PISTEEST� MAALIIN 
int World::calculateHeuristic(RoadNode *node, int goalX, int goalY){ 
    int h; 
    int x = node->x, y = node->y; 
    int xDistance = abs(x-goalX); 
    int yDistance = abs(y-goalY); 
      
    if(xDistance > yDistance){ 
        h = (14 * yDistance) + (10*(xDistance-yDistance)); 
    } 
    else{ 
        h = (14 * xDistance) + (10*(yDistance-xDistance)); 
    } 
  
    return h; 
} 
  
//LASKEE HINNAN TIETYLLE PISTEELLE, OTTAA HUOMIOON MAASTON 
int World::calculateCost(RoadNode *node, RoadNode *parent){ 
    int parentCost = 0;  
      
    if(node->parent != NULL){ 
        parentCost = node->parent->G; 
    } 
    if(isAdjacentPenaltyNode(Coordinate(node->x,node->y))){ 
        parentCost += 5; 
    } 
  
    switch(map[node->x][node->y].getBlock()){ 
    case WO_SAND: 
        parentCost += 13; 
        break; 
    case WO_FOREST: 
        parentCost += 15; 
        break; 
    case WO_GRASS: 
        parentCost += 13; 
        break; 
    case WO_ROAD: 
        break; 
    case WO_LARGE_ROAD: 
        break; 
    case WO_BRIDGE: 
        break; 
    case WO_WATER: 
        parentCost += 25; 
        break; 
    case WO_DEEP_WATER: 
        parentCost += 40; 
        break; 
    default: 
        parentCost += 10; 
        break; 
    } 
  
    if(node->x != parent->x && node->y != parent->y){ 
        return parentCost+14;    
    } 
    else{ 
        return parentCost+10; 
    } 
} 
  
//TARKASTAA ONKO L�HELL� KIELLETTY� MAASTOA 
//T�LL� ESTET��N TEIDEN "KALLIONHALAAMINEN" 
bool World::isAdjacentPenaltyNode(Coordinate c){ 
    int x = c.x, y = c.y; 
    if(x > 1 && x < size-2 && y > 1 && y < size-2){ 
        if(isPenaltyNode(Coordinate(x+1,y))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x-1,y))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x,y+1))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x,y-1))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x+1,y+1))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x-1,y-1))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x+1,y-1))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x-1,y+1))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x+2,y))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x-2,y))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x,y+2))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x,y-2))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x+2,y+2))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x-2,y-2))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x+2,y-2))){ 
            return true; 
        } 
        if(isPenaltyNode(Coordinate(x-2,y+2))){ 
            return true; 
        } 
    } 
    return false; 
} 
  
//TARKASTAA ONKO MAASTO TIEN VIERUSTALLE EP�SOPIVAA 
bool World::isPenaltyNode(Coordinate c){ 
    switch(map[c.x][c.y].getBlock()){ 
    case WO_LAND: 
        return false; 
    case WO_LAND_ALT: 
        return false; 
    case WO_ROAD: 
        return false; 
    case WO_LARGE_ROAD: 
        return false; 
    case WO_BRIDGE: 
        return false; 
    case WO_HI_LAND: 
        return false; 
    case WO_HI_LAND_ALT: 
        return false; 
    case WO_SAND: 
        return false; 
    case WO_FOREST: 
        return false; 
    case WO_GRASS: 
        return false; 
    case WO_ROOF_ASCIICENTER: 
        return false; 
    case WO_ROOF_GYM: 
        return false; 
    case WO_ROOF_NORMAL_HOUSE: 
        return false; 
    case WO_ROOF_SHOP: 
        return false; 
    case WO_ROOF_SPAWN: 
        return false; 
    case WO_DOOR_ASCIICENTER: 
        return false; 
    case WO_DOOR_GYM: 
        return false; 
    case WO_DOOR_NORMAL_HOUSE: 
        return false; 
    case WO_DOOR_SHOP: 
        return false; 
    case WO_DOOR_SPAWN: 
        return false; 
    case WO_WALL: 
        return false; 
    case WO_DEBUG: 
        return false; 
    default: 
        return true; 
    } 
} 
  
//TARKASTAA KYSEENOMAISEN NODEN, JOS NODE ON AVOINNA JA SINNE TULISI HALVEMPI REITTI NYKYISEN NODEN KAUTTA 
//KUIN KYSEENOMAISEN NODEN KAUTTA, VAIHTAA KYSEENOMAISEN NODEN VANHEMMAKSI ITSENS� 
void World::checkNode(RoadNode *node, RoadNode *parent){ 
    if(!node->closed){ 
        if(node->open){ 
            if(node->G > calculateCost(node,parent)){ 
                node->parent = parent; 
            } 
        } 
    } 
} 
  
//ALUSTAA NODEN, JOS NODE ON TIELLE SOPIMATON NIIN SE SULJETAAN 
//LASKEE MY�S HINNAT NODELLE 
bool World::formatNode(RoadNode *node, RoadNode *parent, int x, int y, int goalX, int goalY){ 
    if(x < 0 || x > size-1 || y < 0 || y > size-1){ 
        node->closed = true; 
        return false; 
    } 
    switch(map[x][y].getBlock()){ 
    case WO_LAND: 
        break; 
    case WO_LAND_ALT: 
        break; 
    case WO_HI_LAND: 
        break; 
    case WO_HI_LAND_ALT: 
        break; 
    case WO_ROAD: 
        break; 
    case WO_LARGE_ROAD: 
        break; 
    case WO_BRIDGE: 
        break; 
    case WO_SAND: 
        break; 
    case WO_FOREST: 
        break; 
    case WO_GRASS_ALT: 
        break; 
    case WO_GRASS: 
        break; 
    case WO_WATER: 
        break; 
    case WO_DEEP_WATER: 
        break; 
    case WO_DEBUG: 
        break; 
    default: 
        node->closed = true; 
        return false; 
    } 
  
    if(!node->closed){ 
        if(!node->open){ 
            node->open = true; 
            node->x = x; 
            node->y = y; 
            node->parent = parent; 
            node->G = calculateCost(node,parent); 
            node->H = calculateHeuristic(node,goalX,goalY); 
            node->F = node->G+node->H; 
            return true; 
        } 
    } 
    return false; 
} 
  
void World::generateBuilding(Building *building){ 
    int sizeX = building->getSizeX(); 
    int sizeY = building->getSizeY(); 
    int doorX = building->getDoorX(); 
    int doorY = building->getDoorY(); 
    int x = building->getLocationX(); 
    int y = building->getLocationY(); 
    int curX, curY; 
    WorldBlock temp; 
  
    for(int i = 0 ; i < sizeX ;  i++){ 
        for(int j = 0 ; j < sizeY ; j++){ 
            curX = x-doorX+i; 
            curY = y-doorY+j; 
            temp = WorldBlock(building->getTileOutside(i,j)); 
  
            map[curX][curY] = temp; 
        } 
    } 
} 
  
void World::generateBuilding(int x, int y, Building *building){ 
    int sizeX = building->getSizeX(); 
    int sizeY = building->getSizeY(); 
    int doorX = building->getDoorX(); 
    int doorY = building->getDoorY(); 
    int curX, curY; 
    WorldBlock temp; 
  
    for(int i = 0 ; i < sizeX ;  i++){ 
        for(int j = 0 ; j < sizeY ; j++){ 
            curX = x-doorX+i; 
            curY = y-doorY+j; 
            temp = WorldBlock(building->getTileOutside(i,j)); 
  
            map[curX][curY] = temp; 
        } 
    } 
} 
  
bool World::isPassableArea(int x, int y, int offset){ 
    if(x >= offset && x < size - offset && y >= offset && y < size - offset){ 
        for(int i = 0 ; i <= offset ; i++){ 
            for(int j = 0 ; j <= offset ; j++){               
                if(!isPassable(x+i,y+j) || !isPassable(x+i,y-j) || !isPassable(x-i,y-j) || !isPassable(x-i,y+j)){ 
                    return false; 
                } 
            }            
        } 
        return true; 
    }else
        return false; 
} 
  
bool World::isPassable(int x, int y){ 
    switch(map[x][y].getBlock()){ 
    case WO_LAND: 
        return true; 
    case WO_LAND_ALT: 
        return true; 
    case WO_HI_LAND: 
        return true; 
    case WO_HI_LAND_ALT: 
        return true; 
    default: 
        return false; 
    } 
} 
  
void World::mapThread(vector<vector<int>> *v, int x1, int x2){ 
    if(x1 > size-1){ 
        return; 
    } 
  
    int sizeX = abs(x1-x2); 
  
    for(int i = x1, j = 0 ; j <= sizeX ; i++, j++){ 
        for(int k = 0 ; k < size ; k++){ 
            (*v)[j][k] = int((islandGradient(i,k) + (pn->GetHeight(i,k)+128))/2); 
        } 
    } 
} 
  
void World::smoothenMap(){ 
    WORLDOBJ currentBlock; 
    int sameBlocks, blockHeight; 
  
    for(int i = 1 ; i < size-1 ; i++){ 
        for(int j = 1 ; j < size-1 ; j++){ 
            sameBlocks = 0; 
            currentBlock = map[i][j].getBlock(); 
            if(currentBlock != map[i+1][j].getBlock()){ 
                sameBlocks++; 
            } 
            if(currentBlock != map[i-1][j].getBlock()){ 
                sameBlocks++; 
            } 
            if(currentBlock != map[i][j+1].getBlock()){ 
                sameBlocks++; 
            } 
            if(currentBlock != map[i][j-1].getBlock()){ 
                sameBlocks++; 
            } 
            if(sameBlocks > 1){ 
                blockHeight = (heightmap[i+1][j] + heightmap[i-1][j] + heightmap[i][j+1] + heightmap[i][j-1])/4; 
                map[i][j] = WorldBlock(getTileType(blockHeight)); 
            } 
        } 
    } 
} 
  
WORLDOBJ World::getTileType(int height){ 
    if(height < deepWaterLevel){ 
        return WO_DEEP_WATER; 
    } 
    if(height < waterLevel){ 
        return WO_WATER; 
    } 
    if(height < sandLevel){ 
        return WO_SAND; 
    } 
    if(height < landLevel){ 
        if(rand()%100 > 49){ 
            return WO_LAND; 
        }else{ 
            return WO_LAND_ALT; 
        } 
    } 
    if(height < hiLandLevel){ 
        if(rand()%100 > 10){ 
            return WO_HI_LAND; 
        }else{ 
            return WO_HI_LAND_ALT; 
        } 
    } 
    if(height < hillLevel){ 
        return WO_HILL; 
    } 
    return WO_SNOW; 
} 
  
void World::calculateLevels(){ 
    float deepWaterPercentage = 52; 
    float waterPercentage = 70; 
    float sandPercentage = 74; 
    float landPercentage = 87; 
    float hiLandPercentage = 96; 
    float hillPercentage = 99.5; 
    int index,size; 
      
    sort(heights.begin(),heights.end()); 
  
    size = heights.size(); 
  
    index = float(size*deepWaterPercentage/100); 
    deepWaterLevel = heights[index]; 
  
    index = float(size*waterPercentage/100); 
    waterLevel = heights[index]; 
  
    index = float(size*sandPercentage/100); 
    sandLevel = heights[index]; 
  
    index = float(size*landPercentage/100); 
    landLevel = heights[index]; 
  
    index = float(size*hiLandPercentage/100); 
    hiLandLevel = heights[index]; 
  
    index = float(size*hillPercentage/100);  
    hillLevel = heights[index]; 
} 
  
double World::islandGradient(int x, int y){ 
    double distanceToCenter; 
    double distanceX, distanceY; 
    double largestDistance = sqrt(2*pow(size/2,2))/2; 
  
    int centerX = size/2; 
    int centerY = size/2; 
      
    distanceX = pow((centerX - x),2); 
    distanceY = pow((centerY - y),2); 
  
    distanceToCenter = sqrt(distanceX + distanceY); 
  
    distanceToCenter = (distanceToCenter - largestDistance)/largestDistance; 
    distanceToCenter = distanceToCenter * 255; 
    if(distanceToCenter > 255){ 
        distanceToCenter = 255; 
    } 
    else if(distanceToCenter < 0){ 
        distanceToCenter = 0; 
    } 
  
    return 255-distanceToCenter; 
} 
  
WORLDOBJ World::getBlock(int y, int x){ 
    if(y > -1 && y < size && x > -1 && x < size){  
        return map[y][x].getBlock(); 
    } 
    return WORLDOBJ::WO_DEEP_WATER; 
} 
  
void World::makeMapBmp(string path){     
  
    ofstream out = ofstream("mapit/"+path, ios::binary); 
    const int BMP_FILE_HEADER_SIZE = 14; 
    const int BMP_INFO_HEADER_SIZE = 40; 
  
    if(!out.is_open()){ 
        system("mkdir \mapit"); 
        out = ofstream("mapit/"+path, ios::binary); 
    } 
  
    //BMP HEADER: 
  
    out.put('B'); 
    out.put('M'); 
  
    int fileSize = size*size*3 + BMP_FILE_HEADER_SIZE + BMP_INFO_HEADER_SIZE; 
  
    out.write((const char *)&fileSize, sizeof(int)); 
  
    short reserved = 0; 
  
    out.write((const char *)&reserved, sizeof(short)); 
    out.write((const char *)&reserved, sizeof(short)); 
  
    int offset = BMP_FILE_HEADER_SIZE + BMP_INFO_HEADER_SIZE; 
     
    out.write((const char *)&offset, sizeof(int)); 
  
    int headerSize = BMP_INFO_HEADER_SIZE; 
     
    out.write((const char *)&headerSize, sizeof(int)); 
  
    out.write((const char *)&size, sizeof(int)); 
    out.write((const char *)&size, sizeof(int)); 
  
    short colorPlanes = 1; 
  
    out.write((const char *)&colorPlanes, sizeof(short)); 
  
    short bitsPerPixel = 24; 
  
    out.write((const char *)&bitsPerPixel, sizeof(short)); 
  
    int zero = 0; 
  
    for(int i = 0 ; i < 6 ; i++){ 
        out.write((const char *)&zero, sizeof(int)); 
    } 
  
    //Itse kartan piirto: 
  
    for(int i = size-1 ; i > -1 ; i--){ 
        for(int j = 0 ; j < size ; j++){ 
            writePixel(out, map[j][i].getBlock()); 
        } 
    } 
      
    out.close(); 
} 
  
void World::makeHMapBmp(){ 
    ofstream out("mapit/hmap.bmp", ios::binary); 
    const int BMP_FILE_HEADER_SIZE = 14; 
    const int BMP_INFO_HEADER_SIZE = 40; 
  
    //BMP HEADER: 
  
    out.put('B'); 
    out.put('M'); 
  
    int fileSize = size*size*3 + BMP_FILE_HEADER_SIZE + BMP_INFO_HEADER_SIZE; 
  
    out.write((const char *)&fileSize, sizeof(int)); 
  
    short reserved = 0; 
  
    out.write((const char *)&reserved, sizeof(short)); 
    out.write((const char *)&reserved, sizeof(short)); 
  
    int offset = BMP_FILE_HEADER_SIZE + BMP_INFO_HEADER_SIZE; 
     
    out.write((const char *)&offset, sizeof(int)); 
  
    int headerSize = BMP_INFO_HEADER_SIZE; 
     
    out.write((const char *)&headerSize, sizeof(int)); 
  
    out.write((const char *)&size, sizeof(int)); 
    out.write((const char *)&size, sizeof(int)); 
  
    short colorPlanes = 1; 
  
    out.write((const char *)&colorPlanes, sizeof(short)); 
  
    short bitsPerPixel = 24; 
  
    out.write((const char *)&bitsPerPixel, sizeof(short)); 
  
    int zero = 0; 
  
    for(int i = 0 ; i < 6 ; i++){ 
        out.write((const char *)&zero, sizeof(int)); 
    } 
  
    //Itse kartan piirto: 
  
    unsigned char color[3]; 
  
    for(int i = 0 ; i < size ; i++){ 
        for(int j = 0 ; j < size ; j++){ 
            for(int k = 0 ; k < 3 ; k++){ 
                color[k] = islandGradient(i,j); 
            } 
            out.write((const char*)&color, sizeof(unsigned char)*3); 
        } 
    } 
      
    out.close(); 
} 
  
void World::makePNHMapBmp(){ 
    ofstream out("mapit/pnhmap.bmp", ios::binary); 
    const int BMP_FILE_HEADER_SIZE = 14; 
    const int BMP_INFO_HEADER_SIZE = 40; 
  
    //BMP HEADER: 
  
    out.put('B'); 
    out.put('M'); 
  
    int fileSize = size*size*3 + BMP_FILE_HEADER_SIZE + BMP_INFO_HEADER_SIZE; 
  
    out.write((const char *)&fileSize, sizeof(int)); 
  
    short reserved = 0; 
  
    out.write((const char *)&reserved, sizeof(short)); 
    out.write((const char *)&reserved, sizeof(short)); 
  
    int offset = BMP_FILE_HEADER_SIZE + BMP_INFO_HEADER_SIZE; 
     
    out.write((const char *)&offset, sizeof(int)); 
  
    int headerSize = BMP_INFO_HEADER_SIZE; 
     
    out.write((const char *)&headerSize, sizeof(int)); 
  
    out.write((const char *)&size, sizeof(int)); 
    out.write((const char *)&size, sizeof(int)); 
  
    short colorPlanes = 1; 
  
    out.write((const char *)&colorPlanes, sizeof(short)); 
  
    short bitsPerPixel = 24; 
  
    out.write((const char *)&bitsPerPixel, sizeof(short)); 
  
    int zero = 0; 
  
    for(int i = 0 ; i < 6 ; i++){ 
        out.write((const char *)&zero, sizeof(int)); 
    } 
  
    //Itse kartan piirto: 
  
    unsigned char color[3]; 
    int temp; 
  
    for(int i = 0 ; i < size ; i++){ 
        for(int j = 0 ; j < size ; j++){ 
            temp = pn->GetHeight(i,j)+128; 
            for(int k = 0 ; k < 3 ; k++){ 
                color[k] = temp; 
            } 
            for(int i = 0 ; i < 3 ; i++){ 
                out.write((const char*)&color, sizeof(unsigned char)); 
            } 
        } 
    } 
      
    out.close(); 
} 
  
//WORLDBLOCK 
  
WorldBlock::WorldBlock(){ 
    block = WO_BLANK; 
    building = NULL; 
} 
  
WorldBlock::WorldBlock(Building *building){ 
    this->building = building; 
    block = building->getTileOutside(building->getDoorX(),building->getDoorY()); 
} 
  
WorldBlock::WorldBlock(WORLDOBJ block){ 
    this->block = block; 
    building = NULL; 
} 
  
Building* WorldBlock::getBuilding(){ 
    return building; 
} 
  
WORLDOBJ WorldBlock::getBlock(){ 
    return block; 
} 
  
void writePixel(ofstream &out, WORLDOBJ pixel){ 
    unsigned char color[] = {0,0,0}; 
  
    //0 = BLUE 
    //1 = GREEN 
    //2 = RED 
  
    switch(pixel){ 
        case WO_DEBUG: 
            color[0] = 0; 
            color[1] = 0; 
            color[2] = 0; 
            break; 
        case WO_DEEP_WATER: 
            color[0] = 150+rand()%15; 
            break; 
        case WO_WATER: 
            color[0] = 200+rand()%15; 
            break; 
        case WO_SAND: 
            color[1] = 240+rand()%10; 
            color[2] = 240+rand()%10; 
            break; 
        case WO_LAND: 
            color[1] = 200+rand()%10; 
            break; 
        case WO_LAND_ALT: 
            color[1] = 200+rand()%10; 
            break; 
        case WO_HILL: 
            color[0] = 100+rand()%5; 
            color[1] = 100+rand()%5; 
            color[2] = 100+rand()%5; 
            break; 
        case WO_HI_LAND: 
            color[1] = 180+rand()%10; 
            break; 
        case WO_HI_LAND_ALT: 
            color[1] = 180+rand()%10; 
            break; 
        case WO_SNOW: 
            color[0] = 240+rand()%5; 
            color[1] = 240+rand()%5; 
            color[2] = 240+rand()%5; 
            break; 
        case WO_ROOF_NORMAL_HOUSE: 
            color[1] = 97; 
            color[2] = 153; 
            break; 
        case WO_DOOR_NORMAL_HOUSE: 
            color[1] = 67; 
            color[2] = 106; 
            break; 
        case WO_ROOF_SPAWN: 
            color[1] = 97; 
            color[2] = 153; 
            break; 
        case WO_DOOR_SPAWN: 
            color[1] = 67; 
            color[2] = 106; 
            break; 
        case WO_ROOF_GYM: 
            color[0] = 220; 
            color[1] = 90; 
            color[2] = 30; 
            break; 
        case WO_DOOR_GYM: 
            color[1] = 67; 
            color[2] = 106; 
            break; 
        case WO_ROOF_ASCIICENTER: 
            color[0] = 20; 
            color[2] = 200; 
            break; 
        case WO_DOOR_ASCIICENTER: 
            color[1] = 67; 
            color[2] = 106; 
            break; 
        case WO_ROOF_SHOP: 
            color[0] = 0; 
            color[1] = 180; 
            color[2] = 220; 
            break; 
        case WO_DOOR_SHOP: 
            color[1] = 67; 
            color[2] = 106; 
            break; 
        case WO_WALL: 
            color[0] = 187; 
            color[1] = 187; 
            color[2] = 187; 
            break; 
        case WO_BRIDGE: 
            color[0] = 200; 
            color[1] = 200; 
            color[2] = 200; 
            break; 
        case WO_ROAD: 
            color[0] = 30; 
            color[1] = 70; 
            color[2] = 90; 
            break; 
        case WO_LARGE_ROAD: 
            color[0] = 30; 
            color[1] = 70; 
            color[2] = 90; 
            break; 
        case WO_GRASS: 
            color[0] = 20+rand()%5; 
            color[1] = 150+rand()%5; 
            color[2] = 20+rand()%5; 
            break; 
        case WO_GRASS_ALT: 
            color[0] = 20+rand()%5; 
            color[1] = 150+rand()%5; 
            color[2] = 20+rand()%5; 
            break; 
        case WO_FOREST: 
            color[0] = 20+rand()%5; 
            color[1] = 130+rand()%5; 
            color[2] = 20+rand()%5; 
            break; 
    } 
    for(int i = 0 ; i < 3 ; i++){ 
        out.write((const char*)&color[i], sizeof(unsigned char)); 
    } 
}