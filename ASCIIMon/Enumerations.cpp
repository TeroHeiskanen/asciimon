#include <iostream>
#include <fstream>
#include <string>
#include "Enumerations.h"

ostream& operator<<(ostream& out, MONSTERTYPE& t){
	if(t == MT_NORMAL)
		out<<"NORMAL";
	else if(t == MT_WATER)
		out<<"WATER";
	else if(t == MT_FIRE)
		out<<"FIRE";
	else if(t == MT_GRASS)
		out<<"GRASS";
	else if(t == MT_ELECTRIC)
		out<<"ELECTRIC";
	else if(t == MT_PSYCHIC)
		out<<"PSYCHIC";
	else if(t == MT_FIGHTING)
		out<<"FIGHTING";
	else if(t == MT_GROUND)
		out<<"GROUND";
	else if(t == MT_ICE)
		out<<"ICE";
	else if(t == MT_POISON)
		out<<"POISON";
	else if(t == MT_BUG)
		out<<"BUG";
	return out;
}

istream& operator>>(istream& in, MONSTERTYPE& t){
	string temp;
	in>>temp;
	if(temp == "NORMAL")
		t = MT_NORMAL;
	else if(temp == "WATER")
		t = MT_WATER;
	else if(temp == "FIRE")
		t = MT_FIRE;
	else if(temp == "GRASS")
		t = MT_GRASS;
	else if(temp == "ELECTRIC")
		t = MT_ELECTRIC;
	else if(temp == "PSYCHIC")
		t = MT_PSYCHIC;
	else if(temp == "FIGHTING")
		t = MT_FIGHTING;
	else if(temp == "GROUND")
		t = MT_GROUND;
	else if(temp == "ICE")
		t = MT_ICE;
	else if(temp == "POISON")
		t = MT_POISON;
	else if(temp == "BUG")
		t = MT_BUG;
	return in;	
}

fstream& operator>>(fstream& in, MONSTERTYPE& t){
	string temp;
	in>>temp;
	if(temp == "NORMAL")
		t = MT_NORMAL;
	else if(temp == "WATER")
		t = MT_WATER;
	else if(temp == "FIRE")
		t = MT_FIRE;
	else if(temp == "GRASS")
		t = MT_GRASS;
	else if(temp == "ELECTRIC")
		t = MT_ELECTRIC;
	else if(temp == "PSYCHIC")
		t = MT_PSYCHIC;
	else if(temp == "FIGHTING")
		t = MT_FIGHTING;
	else if(temp == "GROUND")
		t = MT_GROUND;
	else if(temp == "ICE")
		t = MT_ICE;
	else if(temp == "POISON")
		t = MT_POISON;
	else if(temp == "BUG")
		t = MT_BUG;
	return in;	
}