#include "Variables.h"
#include <curses.h>
#include "Icon.h"
#include "World.h"

using namespace std;

class Window{
private:
	WINDOW *worldWin, *combatWin, *infoWin;
	ASCIIMonster *enemy;
	MonsterSet *playerSet;
	int playerPositionX, playerPositionY, maxHeight, maxWidth, menuX, menuY, menuMX, menuMY, menuCY, miniMenuY, x, y, moveCount, activePlayerMon, tmp, seed;
	bool combatWinActive;
	bool worldWinActive;
	bool monMenuActive;
	bool fightMenuActive;
	bool idMenuActive;
	bool mainMenuActive;
	bool monMenuStatsActive;
	bool monMenuStatsPage;
	bool monListMiniMenuActive;
	bool playerTurn;
	void borders();
	char direction;
	chtype battleFloor;
public:
	Window();
	void initialize();
	void print(WINDOW *win, int x, int y, Icon icon);
	void drawOpponent(ASCIIMonster *m);
	void drawPlayer(ASCIIMonster *m);
	void drawMonList(MonsterSet *playerSet);
	void drawMonListCursor();
	void drawCombatMenu();
	void drawCombat();
	void drawPlayerMon(int m);
	void drawEnemyMon();
	void drawMoves();
	void clearWorldWin();
	void clearCombatWin();
	void clearCombatMenu();
	void clearInfoWin();
	void clickNext();
	void battleTurn();
	void checkHp();
	void refreshFix();
	void activateBattle(int activePlayerMon);
	void drawWorld(int x, int y);
	void moveMap();
	void getKeyPress();
	void initColors();
	void info();
	void runAway();
	void combat();
	void drawMonListMiniMenu();
	bool collisionDetect(char direction, int x, int y);
	void playerMove();
};