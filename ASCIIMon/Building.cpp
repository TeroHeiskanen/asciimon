#include "Building.h"
#include <iostream>

using namespace std;

int distance(int x1, int y1, int x2, int y2);

//BUILDING

Building::Building(){
}

Building::Building(HOUSE type, int sizeX, int sizeY, int locationX, int locationY){
	buildingType = type;
	this->sizeX = sizeX;
	this->sizeY = sizeY;
	this->locationX = locationX;
	this->locationY = locationY;
	doorX = sizeX/2;
	doorY = sizeY-1;

	inside = new WORLDOBJ*[sizeX];
	
	for(int i = 0 ; i < sizeX ; i++){
		inside[i] = new WORLDOBJ[sizeY];
	}

	outside = new WORLDOBJ*[sizeX];

	for(int i = 0 ; i < sizeX ; i++){
		outside[i] = new WORLDOBJ[sizeY];
	}
}

Building::Building(HOUSE type, int sizeX, int sizeY, int doorX, int doorY, int locationX, int locationY){
	buildingType = type;
	this->sizeX = sizeX;
	this->sizeY = sizeY;
	this->doorX = doorX;
	this->doorY = doorY;
	this->locationX = locationX;
	this->locationY = locationY;

	inside = new WORLDOBJ*[sizeX];
	
	for(int i = 0 ; i < sizeX ; i++){
		inside[i] = new WORLDOBJ[sizeY];
	}

	outside = new WORLDOBJ*[sizeY];

	for(int i = 0 ; i < sizeX ; i++){
		outside[i] = new WORLDOBJ[sizeY];
	}
}

int Building::getDoorX(){
	return doorX;
}

int Building::getDoorY(){
	return doorY;
}

int Building::getLocationX(){
	return locationX;
}

int Building::getLocationY(){
	return locationY;
}

int Building::getSizeX(){
	return sizeX;
}

int Building::getSizeY(){
	return sizeY;
}

WORLDOBJ Building::getTileInside(int x, int y){
	if(x < 0 || x > sizeX-1 || y < 0 || y > sizeY-1){
		return WO_BLANK;
	}
	else{
		return inside[x][y];
	}
}

WORLDOBJ Building::getTileOutside(int x, int y){
	if(x < 0 || x > sizeX-1 || y < 0 || y > sizeY-1){
		return WO_BLANK;
	}
	else{
		return outside[x][y];
	}
}

ASCIICenter::ASCIICenter(int locationX, int locationY):Building(H_ASCIICENTER, 5, 4, locationX, locationY){
	for(int i = 0 ; i < sizeX ; i++){
		for(int j = 0 ; j < sizeY-2 ; j++){
			outside[i][j] = WO_ROOF_ASCIICENTER;
		}
	}
	for(int i = 0 ; i < sizeX ; i++){
		outside[i][sizeY-1] = WO_WALL;
		outside[i][sizeY-2] = WO_WALL;
	}
	outside[sizeX/2][sizeY-1] = WO_DOOR_ASCIICENTER;
}

NormalHouse::NormalHouse(int locationX, int locationY):Building(H_NORMAL_HOUSE, 5, 3, locationX, locationY){
	for(int i = 0 ; i < sizeX ; i++){
		for(int j = 0 ; j < sizeY-1 ; j++){
			outside[i][j] = WO_ROOF_NORMAL_HOUSE;
		}
	}
	for(int i = 0 ; i < sizeX ; i++){
		outside[i][sizeY-1] = WO_WALL;
	}
	outside[sizeX/2][sizeY-1] = WO_DOOR_NORMAL_HOUSE;
}

SpawnHouse::SpawnHouse(int locationX, int locationY):Building(H_SPAWN, 3, 3, locationX, locationY){
	for(int i = 0 ; i < sizeX ; i++){
		for(int j = 0 ; j < sizeY-1 ; j++){
			outside[i][j] = WO_ROOF_SPAWN;
		}
	}
	for(int i = 0 ; i < sizeX ; i++){
		outside[i][sizeY-1] = WO_WALL;
	}
	outside[sizeX/2][sizeY-1] = WO_DOOR_SPAWN;
}

Gym::Gym(int locationX, int locationY):Building(H_GYM, 7, 4, 5, 3, locationX, locationY){
	for(int i = 0 ; i < sizeX ; i++){
		for(int j = 0 ; j < sizeY-2 ; j++){
			outside[i][j] = WO_ROOF_GYM;
		}
	}
	for(int i = 0 ; i < sizeX ; i++){
		outside[i][sizeY-1] = WO_WALL;
		outside[i][sizeY-2] = WO_WALL;
	}
	outside[sizeX-2][sizeY-1] = WO_DOOR_GYM;
}

Shop::Shop(int locationX, int locationY):Building(H_SHOP, 3, 4, locationX, locationY){
	for(int i = 0 ; i < sizeX ; i++){
		for(int j = 0 ; j < sizeY-2 ; j++){
			outside[i][j] = WO_ROOF_SHOP;
		}
	}
	for(int i = 0 ; i < sizeX ; i++){
		outside[i][sizeY-1] = WO_WALL;
		outside[i][sizeY-2] = WO_WALL;
	}
	outside[sizeX/2][sizeY-1] = WO_DOOR_SHOP;
}

//CITY

City::City(){
	buildingCount = 0;
}

City::City(CITY cityType, int radius, int roadX, int roadY){
	this->cityType = cityType;
	this->radius = radius;
	this->roadX = roadX;
	this->roadY = roadY;
	buildingCount = 0;
}

int City::getRadius(){
	return radius;
}

int City::getRoadX(){
	return roadX;
}

int City::getRoadY(){
	return roadY;
}

int City::getBuildingCount(){
	return buildingCount;
}

Building* City::getBuilding(int index){
	if(index >= 0 && index < buildingCount){
		return &buildings[index];
	}
	return NULL;
}

CITY City::getCityType(){
	return cityType;
}

bool City::isValidPosition(Building building1, Building building2){
	int gap = 1;
	int distance;
	int distanceBetweenDoors;
	int b1DistanceToWall, b2DistanceToWall;
	bool xOverlap = false, yOverlap = false;
	
	int b1X = building1.getLocationX(), b1Y = building1.getLocationY();
	int b2X = building2.getLocationX(), b2Y = building2.getLocationY();

	int b1DoorX = building1.getDoorX(), b1DoorY = building1.getDoorY();
	int b2DoorX = building2.getDoorX(), b2DoorY = building2.getDoorY();

	int b1SizeX = building1.getSizeX(), b1SizeY = building1.getSizeY();
	int b2SizeX = building2.getSizeX(), b2SizeY = building2.getSizeY();
	
	if(b1X < b2X){
		distanceBetweenDoors = b2X-b1X-1;
		b1DistanceToWall = b1SizeX-b1DoorX-1;
		b2DistanceToWall = b2DoorX;
		distance = distanceBetweenDoors - b1DistanceToWall - b2DistanceToWall;
		if(distance < gap){
			xOverlap = true;
		}
	}
	else if(b1X > b2X){
		distanceBetweenDoors = b1X-b2X-1;
		b1DistanceToWall = b1DoorX;
		b2DistanceToWall = b2SizeX-b2DoorX-1;
		distance = distanceBetweenDoors - b1DistanceToWall - b2DistanceToWall;
		if(distance < gap){
			xOverlap = true;
		}
	}
	else if(b1X == b2X){
		xOverlap = true;
	}
	if(b1Y < b2Y){
		distanceBetweenDoors = b2Y-b1Y-1;
		b1DistanceToWall = 0;
		b2DistanceToWall = b2SizeY-1;
		distance = distanceBetweenDoors - b1DistanceToWall - b2DistanceToWall;
		if(distance < gap){
			yOverlap = true;
		}
	}
	else if(b1Y > b2Y){
		distanceBetweenDoors = b1Y-b2Y-1;
		b1DistanceToWall = b1SizeY-1;
		b2DistanceToWall = 0;
		distance = distanceBetweenDoors - b1DistanceToWall - b2DistanceToWall;
		if(distance < gap){
			yOverlap = true;
		}
	}
	else if(b1Y == b2Y){
		yOverlap = true;
	}

	if(xOverlap && yOverlap){
		return false;
	}
	else{
		return true;
	}
}

void City::addConnection(City *city){
	citiesConnected.push_back(city);
}

int City::getConnectionCount(){
	return citiesConnected.size();
}

City* City::getConnection(int index){
	if(index < citiesConnected.size()){
		return citiesConnected[index]; 
	}
	else{
		return NULL;
	}
}

void City::shareConnections(City *city){
	bool found;
	City *temp;
	for(int i = 0 ; i < citiesConnected.size() ; i++){
		temp = citiesConnected[i];
		found = false;
		for(int j = 0 ; j < city->citiesConnected.size() ; j++){
			if(*temp == *city->citiesConnected[j]){
				found = true;
				break;
			}
		}
		if(!found){
			city->addConnection(temp);
		}
	}
}

bool City::isConnected(City *city){
	for(int i = 0 ; i < citiesConnected.size() ; i++){
		if(*citiesConnected[i] == *city){
			return true;
		}
	}
	return false;
}

bool City::operator==(City &o){
	if(roadX == o.roadX && roadY == o.roadY)
		return true;
	else
		return false;
}

bool City::operator!=(City &o){
	if(roadX != o.roadX || roadY != o.roadY)
		return true;
	else
		return false;
}

SpawnCity::SpawnCity(int x, int y):City(CI_SPAWN,8,x,y){
	buildingCount = 3;
	int index = 0;
	buildings = new Building[buildingCount];
	bool cont;

	Building temp;
	int curX, curY;

	buildings[index++] = SpawnHouse(x,y-1);
	
	for(int i = 0 ; i < 2 ; i++){
		do{
			cont = false;
			curX = x + rand()%(2*radius)-radius;
			curY = y + rand()%(2*radius)-radius;
			temp = NormalHouse(curX, curY);
			for(int j = 0 ; j < index ; j++){
				if(!isValidPosition(temp, buildings[j])){
					cont = true;
					break;
				}
			}
			if(!cont){
				buildings[index++] = temp;
			}
		}while(cont);
	}
}

SmallCity::SmallCity(int x, int y):City(CI_SMALL,10,x,y){
	buildingCount = 4;
	int index = 0;
	buildings = new Building[buildingCount];
	bool cont;

	Building temp;
	int curX, curY;

	buildings[index++] = ASCIICenter(x,y-1);
	
	for(int i = 0 ; i < 3 ; i++){
		do{
			cont = false;
			curX = x + rand()%(2*radius)-radius;
			curY = y + rand()%(2*radius)-radius;
			temp = NormalHouse(curX, curY);
			for(int j = 0 ; j < index ; j++){
				if(!isValidPosition(temp, buildings[j])){
					cont = true;
					break;
				}
			}
			if(!cont){
				buildings[index++] = temp;
			}
		}while(cont);
	}
}

MediumCity::MediumCity(int x, int y):City(CI_MEDIUM,15,x,y){
	buildingCount = 7;
	int index = 0;
	buildings = new Building[buildingCount];
	bool cont;

	Building temp;
	int curX, curY;

	buildings[index++] = ASCIICenter(x,y-1);
	
	do{
		cont = false;
		curX = x + rand()%(2*radius)-radius;
		curY = y + rand()%(2*radius)-radius;
		temp = Gym(curX, curY);
		for(int j = 0 ; j < index ; j++){
			if(!isValidPosition(temp, buildings[j])){
				cont = true;
				break;
			}
		}
		if(!cont){
			buildings[index++] = temp;
		}
	}while(cont);

	for(int i = 0 ; i < 4 ; i++){
		do{
			cont = false;
			curX = x + rand()%(2*radius)-radius;
			curY = y + rand()%(2*radius)-radius;
			temp = NormalHouse(curX, curY);
			for(int j = 0 ; j < index ; j++){
				if(!isValidPosition(temp, buildings[j])){
					cont = true;
					break;
				}
			}
			if(!cont){
				buildings[index++] = temp;
			}
		}while(cont);
	}

	do{
		cont = false;
		curX = x + rand()%(2*radius)-radius;
		curY = y + rand()%(2*radius)-radius;
		temp = Shop(curX, curY);
		for(int j = 0 ; j < index ; j++){
			if(!isValidPosition(temp, buildings[j])){
				cont = true;
				break;
			}
		}
		if(!cont){
			buildings[index++] = temp;
		}
	}while(cont);
}

LargeCity::LargeCity(int x, int y):City(CI_MEDIUM,20,x,y){
	buildingCount = 11;
	int index = 0;
	buildings = new Building[buildingCount];
	bool cont;

	Building temp;
	int curX, curY;

	buildings[index++] = ASCIICenter(x,y-1);
	
	do{
		cont = false;
		curX = x + rand()%(2*radius)-radius;
		curY = y + rand()%(2*radius)-radius;
		temp = Gym(curX, curY);
		for(int j = 0 ; j < index ; j++){
			if(!isValidPosition(temp, buildings[j])){
				cont = true;
				break;
			}
		}
		if(!cont){
			buildings[index++] = temp;
		}
	}while(cont);

	for(int i = 0 ; i < 8 ; i++){
		do{
			cont = false;
			curX = x + rand()%(2*radius)-radius;
			curY = y + rand()%(2*radius)-radius;
			temp = NormalHouse(curX, curY);
			for(int j = 0 ; j < index ; j++){
				if(!isValidPosition(temp, buildings[j])){
					cont = true;
					break;
				}
			}
			if(!cont){
				buildings[index++] = temp;
			}
		}while(cont);
	}

	do{
		cont = false;
		curX = x + rand()%(2*radius)-radius;
		curY = y + rand()%(2*radius)-radius;
		temp = Shop(curX, curY);
		for(int j = 0 ; j < index ; j++){
			if(!isValidPosition(temp, buildings[j])){
				cont = true;
				break;
			}
		}
		if(!cont){
			buildings[index++] = temp;
		}
	}while(cont);
}

int distance(int x1, int y1, int x2, int y2){
	return sqrt(pow(abs(x1-x2),2)+pow(abs(y1-y2),2));
}