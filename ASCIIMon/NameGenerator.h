#include <string>
#include <time.h>
#include <conio.h>
#include <windows.h>
#include <iostream>

using namespace std;

char generateVowel();
char generateConsonant();
bool generateBool();
string generateName();

const char vowel[] = {'A','E','I','O','U','Y'};
const char consonants[] = {'B','C','D','F','G','H','J','K','L','M','N','P','Q','R','S','T','V','W','X','Z'};

string generateName(){
	string name;
	int length;	
	
	int lastVowels = 0;
	int lastConsonants = 0;

	length = rand()%3 + 2;

	for(int i = 0 ; i <= length ; i++){
		if(i == 0){
			if(generateBool()){
				name.append(1,generateVowel());
				lastVowels++;
				continue;
			}else{
				name.append(1,generateConsonant());
				lastConsonants++;
				continue;
			}
		}
		if(lastVowels == 2){
			name.append(1,generateConsonant());
			lastConsonants++;
			lastVowels = 0;
			continue;
		}
		if(lastConsonants == 1){
			name.append(1,generateVowel());
			lastVowels++;
			lastConsonants = 0;
			continue;
		}
		if(generateBool()){
			name.append(1,generateVowel());
			lastVowels++;
			lastConsonants = 0;
		}else{
			name.append(1,generateConsonant());
			lastConsonants++;
			lastVowels = 0;
		}		
	}
	name.append("MON");
	return name;
}

bool generateBool(){
	if((rand()%10+1) > 5)
		return true;
	else
		return false;
}

char generateVowel(){
	return vowel[rand()%6];
}

char generateConsonant(){	
	return consonants[rand()%20];
}