#include "Move.h"
#include <random>
#include <iostream>
#include <fstream>

using namespace std;

string addSpaces(string name);

//MOVE

Move::Move(){
}

Move::Move(string name, MONSTERTYPE type, int lvlReq, int dmg, int pp, int accuracy){
	this->name = name;
	this->type = type;
	this->lvlReq = lvlReq;
	this->dmg = dmg;
	this->pp = pp;
	this->accuracy = accuracy;
}

string Move::getName(){
	return name;
}

MONSTERTYPE Move::getType(){
	return type;
}

int Move::getLvlReq(){
	return lvlReq;
}

int Move::getDmg(){
	return dmg;
}

int Move::getPp(){
	return pp;
}

int Move::getAccuracy(){
	return accuracy;
}

bool Move::use(){
	if(pp > 0){
		pp--;
		if(accuracy == 100 || rand()%100+1 < accuracy){
			return true;
		}
	}
	return false;
}

void Move::print(){
	cout<<"NAME   = "<<name<<endl;
	cout<<"TYPE   = "<<type<<endl;
	cout<<"DMG    = "<<dmg<<endl;
	cout<<"PP     = "<<pp<<endl;
	cout<<"ACC    = "<<accuracy<<endl;
	cout<<"LVLREQ = "<<lvlReq<<endl;
}

//MOVES

Moves::Moves(){
	readMoves();
}

void Moves::addMove(Move m){	
	switch(m.getType()){
		case MT_WATER:
			water[water.size()] = m;
			break;
		case MT_FIRE:
			fire[fire.size()] = m;
			break;
		case MT_GRASS:
			grass[grass.size()] = m;
			break;
		case MT_ELECTRIC:
			electric[electric.size()] = m;
			break;
		case MT_NORMAL:
			normal[normal.size()] = m;
			break;
		case MT_PSYCHIC:
			psychic[psychic.size()] = m;
			break;
		case MT_FIGHTING:
			fighting[fighting.size()] = m;
			break;
		case MT_GROUND:
			ground[ground.size()] = m;
			break;
		case MT_ICE:
			ice[ice.size()] = m;
			break;
		case MT_POISON:
			poison[poison.size()] = m;
			break;
		case MT_BUG:
			bug[bug.size()] = m;
			break;
		default:
			break;
	}
}

void Moves::readMoves(){
	string name;
	MONSTERTYPE type;
	int dmg, pp, accuracy, lvlReq;
	ifstream in("moves.dat");
	if(in.is_open()){
		while(in.peek() != EOF){
			if(in.peek() == '/'){
				in.get();
				if(in.peek() == '/'){
					while(in.get() != '\n');
				}
				continue;
			}
			if(in.peek() == ' ' || in.peek() == '\n'){
				while(in.get() != '\n');
				continue;
			}
			in>>name>>type>>dmg>>pp>>accuracy>>lvlReq;
			if(in.fail()){
				cout<<"ERROR";
				return;
			}
			//name.replace
			addMove(Move(addSpaces(name), type, lvlReq, dmg, pp, accuracy));
		}
	}
}

Move* Moves::getMove(MONSTERTYPE type, int lvl){
	int chance = rand()%100;
	if(chance < 25){
		switch(type){
			case MT_WATER:
				for(unsigned int i = 0 ; i < water.size() ; i++){
					if(water[i].getLvlReq() == lvl){
						return &water[i];
					}
				}
				break;
			case MT_FIRE:
				for(unsigned int i = 0 ; i < fire.size() ; i++){
					if(fire[i].getLvlReq() == lvl){
						return &fire[i];
					}
				}
				break;
			case MT_GRASS:
				for(unsigned int i = 0 ; i < grass.size() ; i++){
					if(grass[i].getLvlReq() == lvl){
						return &grass[i];
					}
				}
				break;
			case MT_ELECTRIC:
				for(unsigned int i = 0 ; i < electric.size() ; i++){
					if(electric[i].getLvlReq() == lvl){
						return &electric[i];
					}
				}
				break;
			case MT_PSYCHIC:
				for(unsigned int i = 0 ; i < psychic.size() ; i++){
					if(psychic[i].getLvlReq() == lvl){
						return &psychic[i];
					}
				}
				break;
			case MT_FIGHTING:
				for(unsigned int i = 0 ; i < fighting.size() ; i++){
					if(fighting[i].getLvlReq() == lvl){
						return &fighting[i];
					}
				}
				break;
			case MT_GROUND:
				for(unsigned int i = 0 ; i < ground.size() ; i++){
					if(ground[i].getLvlReq() == lvl){
						return &ground[i];
					}
				}
				break;
			case MT_ICE:
				for(unsigned int i = 0 ; i < ice.size() ; i++){
					if(ice[i].getLvlReq() == lvl){
						return &ice[i];
					}
				}
				break;
			case MT_POISON:
				for(unsigned int i = 0 ; i < poison.size() ; i++){
					if(poison[i].getLvlReq() == lvl){
						return &poison[i];
					}
				}
				break;
			case MT_BUG:
				for(unsigned int i = 0 ; i < bug.size() ; i++){
					if(bug[i].getLvlReq() == lvl){
						return &bug[i];
					}
				}
				break;
			case MT_NORMAL:
				for(unsigned int i = 0 ; i < normal.size() ; i++){
					if(normal[i].getLvlReq() == lvl){
						return &normal[i];
					}
				}
				break;
		}
	}
	if(chance > 24 && chance < 50){
		for(unsigned int i = 0 ; i < normal.size() ; i++){
			if(normal[i].getLvlReq() == lvl){
				return &normal[i];
			}
		}
	}
	return NULL;
}

Move* Moves::getStartingMove(MONSTERTYPE type, int lvl){
	int chance = rand()%100;
	if(chance < 50){
		for(int j = 0 ; j < 10 ; j++){
			switch(type){
				case MT_WATER:
					for(int i = water.size()-1 ; i >= 0 ; i--){
						if(water[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &water[i];
						}
					}
					break;
				case MT_FIRE:
					for(int i = fire.size()-1 ; i >= 0 ; i--){
						if(fire[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &fire[i];
						}
					}
					break;
				case MT_GRASS:
					for(int i = grass.size()-1 ; i >= 0 ; i--){
						if(grass[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &grass[i];
						}
					}
					break;
				case MT_ELECTRIC:
					for(int i = electric.size()-1 ; i >= 0 ; i--){
						if(electric[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &electric[i];
						}
					}
					break;
				case MT_PSYCHIC:
					for(int i = psychic.size()-1 ; i >= 0 ; i--){
						if(psychic[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &psychic[i];
						}
					}
					break;
				case MT_FIGHTING:
					for(int i = fighting.size()-1 ; i >= 0 ; i--){
						if(fighting[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &fighting[i];
						}
					}
					break;
				case MT_GROUND:
					for(int i = ground.size()-1 ; i >= 0 ; i--){
						if(ground[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &ground[i];
						}
					}
					break;
				case MT_ICE:
					for(int i = ice.size()-1 ; i >= 0 ; i--){
						if(ice[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &ice[i];
						}
					}
					break;
				case MT_POISON:
					for(int i = poison.size()-1 ; i >= 0 ; i--){
						if(poison[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &poison[i];
						}
					}
					break;
				case MT_BUG:
					for(int i = bug.size()-1 ; i >= 0 ; i--){
						if(bug[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &bug[i];
						}
					}
					break;
				case MT_NORMAL:
					for(int i = normal.size()-1 ; i >= 0 ; i--){
						if(normal[i].getLvlReq() <= lvl && rand()%100 > 75){
							return &normal[i];
						}
					}
					break;
					
			}
		}
	}
	if(chance > 49){
		for(int j = 0 ; j < 10 ; j++){
			for(int i = normal.size()-1 ; i >= 0 ; i--){
				if(normal[i].getLvlReq() <= lvl && rand()%100 > 75){
					return &normal[i];
				}
			}
		}
	}
	return NULL;
}

void Moves::print(){
	cout<<"NORMAL"<<endl<<endl;
	for(unsigned int i = 0 ; i < normal.size() ; i++){
		normal[i].print();
		cout<<endl;
	}
	cout<<"WATER"<<endl<<endl;
	for(unsigned int i = 0 ; i < water.size() ; i++){
		water[i].print();
		cout<<endl;
	}	
	cout<<"FIRE"<<endl<<endl;
	for(unsigned int i = 0 ; i < fire.size() ; i++){
		fire[i].print();
		cout<<endl;
	}
	cout<<"GRASS"<<endl<<endl;
	for(unsigned int i = 0 ; i < grass.size() ; i++){
		grass[i].print();
		cout<<endl;
	}
	cout<<"ELECTRIC"<<endl<<endl;
	for(unsigned int i = 0 ; i < electric.size() ; i++){
		electric[i].print();
		cout<<endl;
	}
	cout<<"PSYCHIC"<<endl<<endl;
	for(unsigned int i = 0 ; i < psychic.size() ; i++){
		psychic[i].print();
		cout<<endl;
	}
	cout<<"FIGHTING"<<endl<<endl;
	for(unsigned int i = 0 ; i < fighting.size() ; i++){
		fighting[i].print();
		cout<<endl;
	}
	cout<<"GROUND"<<endl<<endl;
	for(unsigned int i = 0 ; i < ground.size() ; i++){
		ground[i].print();
		cout<<endl;
	}
	cout<<"ICE"<<endl<<endl;
	for(unsigned int i = 0 ; i < ice.size() ; i++){
		ice[i].print();
		cout<<endl;
	}
	cout<<"POISON"<<endl<<endl;
	for(unsigned int i = 0 ; i < poison.size() ; i++){
		poison[i].print();
		cout<<endl;
	}
	cout<<"BUG"<<endl<<endl;
	for(unsigned int i = 0 ; i < bug.size() ; i++){
		bug[i].print();
		cout<<endl;
	}
}

string addSpaces(string name){
	for(unsigned int i = 0 ; i < name.length() ; i++){
		if(name[i] == '_')
			name[i] = ' ';		
	}
	return name;
}
