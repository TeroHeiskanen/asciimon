#include "Enumerations.h"
#include <string>
#include "Icon.h"
#include "Move.h"

using namespace std;

#ifndef MONSTER_H
#define MONSTER_H

class MonsterBase{
protected:
	string name;
	int attMulti;
	int defMulti;
	int spdMulti;
	int hpMulti;
	MONSTERTYPE monsterType;
	Icon monsterIcon;
public:
	MonsterBase();
	MonsterBase(string name);
	MonsterBase(const MonsterBase &obj);
	string getName();
	int getAttMulti();
	int getDefMulti();
	int getSpdMulti();
	int getHpMulti();
	MONSTERTYPE getType();
	Icon getIcon();
	void print();
};

class Monsters{
private:
	MonsterBase *m;
	int count;
public:
	Monsters(int count);
	MonsterBase getRandom();
	MonsterBase get(int index);
	int getCount();
};

class ASCIIMonster:MonsterBase{
private:
	int att;
	int def;
	int spd;
	int hp;
	int maxhp;
	int lvl;
	int exp;
	Move* moves[4];
	void levelUp();
public:
	ASCIIMonster();
	ASCIIMonster(MonsterBase monster, int lvl);
	bool checkLevelUp();
	bool addMove(Move* m);
	bool doesExist(Move* m);
	void delMove(int index);
	void switchMove(int index1, int index2);
	void getStartingMoves();
	void addExp(int exp);	
	int getAtt();
	int getDef();
	int getSpd();
	int getHp();
	int getMaxhp();
	int getLvl();
	int getExp();
	void setHpToMax();
	Move* getMove(int index);	
	void receiveDmg(int dmg);	
	void print();
	using MonsterBase::getName;
	using MonsterBase::getType;
	using MonsterBase::getIcon;
};

class MonsterSet{
private:
	map<int,ASCIIMonster> mSet;
	int count;
	int maxSize;
public:
	MonsterSet();
	MonsterSet(int maxSize);
	void addMonster(ASCIIMonster m);
	void deleteMonster(int index);
	void switchMonsters(int index1, int index2);
	bool isEmpty();
	bool isFull();
	ASCIIMonster* getMonster(int index);
	int getCount();
	int getMaxSize();
};
#endif